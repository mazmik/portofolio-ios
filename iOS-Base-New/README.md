## CA iOS Base

MVP architectural design pattern
Support native framework

### Prerequisites

* Swift 4 and above
* Xcode 10 and above

### Installing
* Create new project. **Recomended Directory structure**
```
[Project Name].xcodeproj
  <[Project Directory]>
     |
     |-- AppDelegate.swift
     |-- SceneDelegate.swift (.)
     |-- Component
     |      |
     |      |---- Helper
     |              |--- CALogger.swift
     |              |--- CAPreference.swift
     |      |
     |      |---- Extension
     |              |------ View
     |      |
     |      |---- Base
     |              |--- Repository.swift
     |              |--- CABaseVC.swift
     |              |--- CABaseView.swift
     |              |--- BaseView.swift
     |              |--- BasePresenter.swift
     |              |--- Repository.swift
     |      |
     |      |-- Network
     |              |--- CABaseAuth.swift
     |              |--- CABaseHttpMethod.swift
     |              |--- CABaseRequest.swift
     |              |--- ApiLevel.swift
     |              |--- CABaseResponse.swift
     |              |--- Request.swift
     |              |--- Reachability.swift
     |-- Module
     |      |
     |      |---- Network
     |              |--- CAAuth.swift
     |              |--- CABaseLevel.swift
     |      |
     |      |---- [Module Name]
     |              |--- View
     |              |--- Use Case
     |              |--- Presenter
     |              |--- Model
     |      |
     |      |---- [Module Name]
     |              |--- View
     |              |--- Use Case
     |              |--- Presenter
     |              |--- Model
     |      |
     |      |---- etc.
     |--- Assets        
     |      |
     |      |---- Assets.xcassets
     |--- Supporting Files        
     |      |
     |      |---- Launchscreen.storyboard
     |      |---- Info.plist
     |
     |--- etc.
```
(.) Optional (only available for iOS 13)

### Make sure that every Presenter, View Controller, and View should extend CABase in Component - Base folder
* View
```
protocol LoginView: CABaseView {
   func onEmailError(s: String)
   func onPasswordError(s: String)
   func onLoginSucceed(response: PostLoginOutput)
}
```
* Presenter
```
protocol LoginPresenter: CABasePresenter {
     func onLogin(phone: String, password: String)
}
class LoginPresentation: LoginPresenter {
     private let view: LoginView!
     init(view: LoginView) {
         self.view = view
     }
     func onLogin(phone: String, password: String) { . . . }
}
```
* View Controller
```
class LoginVC: CABaseVC { . . . }
extension LoginVC: LoginView {
  func onEmailError(s: String) { . . . }
  func onPasswordError(s: String) { . . .}
  func onLoginSucceed(response: PostLoginOutput) { . . . }
 }
```
  
### For data modelling, every Repository, Response, API should extend CABase in Component - Network folder
* Repository
```
class LoginRepository: BaseRepository { . . . }
```
* API
```
class PostLoginApi: CABaseRequest {
    var method: CABaseHttpMethod = .POST
    var path: String = “/auth/login”
    var parameters: [String:Any]
    var apiVersion: ApiVersion = .v1
    var shouldWithToken: Bool = false
    (.)
    init(parameters: [String:Any]) {
        self.parameters = parameters
    }
    (.)
}
```
* Model
```
struct PostLoginRequest: Codable { // make it fit with the request requirement }
struct PostLoginBodyResponse: CABaseResponse { // make it fit with the request requirement }
struct PostLoginBodyFullResponse: Codable { // make it fit with the request requirement }
struct PostLoginOutput { // make it fit with the request requirement }
```
(.) = if the request needs parameters, add those lines into your API request

### For network environment inside Module - Network, 
* ApiLevel
```
class CABaseLevel: ApiLevel {
    var dev: String = "https://private-anon-79fa5b52fa-aetra.apiary-mock.com"
    var prod: String = "prod"
}
```
* Auth
```
class CAAuth: CABaseAuth {
    var bearerToken: String? {
        guard let token = CAPreference.getString(forKey: .TOKEN_BASE) else {
            return nil
        }
        return "Bearer \(token)"
    }
    
    func saveToken(token: String) {
        CAPreference.set(value: token, forKey: .TOKEN_BASE)
    }
    
    func removeToken(token: String) {
        CAPreference.set(value: nil, forKey: .TOKEN_BASE)
    }
}
```

### Note:
* BodyResponse is only exposed when getting the response
* Put BodyFullResponse inside BodyResponse object
* Output is only exposed through UI

**For further more, check the example folder**

