//
//  AppDelegate.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupView()
        return true
    }

    private func setupView() {
        window = UIWindow()
        let homeViewController = LoginVC()
        homeViewController.view.backgroundColor = UIColor.red
        let navController = UINavigationController(rootViewController: homeViewController)
        window!.rootViewController = navController
        window!.makeKeyAndVisible()
    }
}

