//
//  HomeView.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

protocol LoginView: CABaseView {
    func onEmailError(s: String)
    func onPasswordError(s: String)
    func onLoginSucceesNatively(response: PostLoginOutput)
}
