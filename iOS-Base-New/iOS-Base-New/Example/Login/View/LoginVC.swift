//
//  HomeVC.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

class LoginVC: CABaseVC {
    
    private var presenter: LoginPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = LoginPresentation(view: self)
//        presenter.onLogin(phone: "0812345678", password: "password")
        presenter.getProfile()
        title = "Login"
        view.tapGesture {
            self.pushVC(ComplaintsVC())
        }
    }

}

extension LoginVC: LoginView {
    func onEmailError(s: String) {
        
    }
    
    func onPasswordError(s: String) {
        
    }
    
    func onLoginSucceesNatively(response: PostLoginOutput) {
        
        let alert = UIAlertController(title: "Success", message: response.firstName + " " + response.lastName, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true)
    }
}
