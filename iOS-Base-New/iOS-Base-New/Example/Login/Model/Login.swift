//
//  LoginWithoutAlamofire.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

struct PostLoginBodyRequest: Codable {
    let phone: String
    let password: String
    let platform: String
    let device_token: String
}

struct PostLoginBodyResponse: CABaseResponse {
    typealias CAOutput = PostLoginBodyFullResponse
    var data: PostLoginBodyFullResponse?
    var meta: CABaseMeta
}

struct PostLoginBodyFullResponse: Codable {
    var customerId: String
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
    var createdAt: Int
    var updatedAt: Int
}

struct PostLoginOutput {
    var customerId: String
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
    var createdAt: Int
    var updatedAt: Int
}

struct GetProfileBodyResponse: CABaseResponse {
    typealias CAOutput = ProfileModel
    var data: ProfileModel?
    var meta: CABaseMeta
}


struct ProfileModel: Codable {
    var id: String?
    var type: String?
    var attributes: ProfileAttribute?
    var message: String?
}

struct ProfileAttribute: Codable {
    let name: String
    let phoneNumber: String
    let identityImage: IdentityModel
    let selfieImage: SelfieModel
    let drivingLicenseImage: DrivingLicenseModel
    var email: String
}

struct IdentityModel: Codable {
    var url: String?
}

struct SelfieModel: Codable {
    var url: String?
}

struct DrivingLicenseModel: Codable {
    var url: String?
}
