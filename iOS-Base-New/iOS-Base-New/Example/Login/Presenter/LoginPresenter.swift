//
//  HomePresenter.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

protocol LoginPresenter: CABasePresenter {
    func onLogin(phone: String, password: String)
    func getProfile()
}
