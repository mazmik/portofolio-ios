//
//  HomePresentation.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

class LoginPresentation: LoginPresenter {
    private let view: LoginView!
    
    init(view: LoginView) {
        self.view = view
    }
    
    func onLogin(phone: String, password: String) {
        let deviceToken: String = "12b3437fhhf37327892383djs4fj3289329274gfwefh38301ke0h383dh28f327"
        let repo: LoginRepository = LoginRepository(level: CABaseLevel())
        view.onLoading(msg: "Loading")
        let nativeRequest: PostLoginBodyRequest = PostLoginBodyRequest(phone: phone, password: password, platform: "ios", device_token: deviceToken)
        repo.onPostLoginNative(body: nativeRequest, onSuccess: { response in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                self.view.onFinishLoading()
                self.view.onLoginSucceesNatively(response: response)
            }
        }) { err in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                self.view.onFinishLoading()
                self.view.showError(msg: err.localizedDescription)
            }
        }
    }
    
    func getProfile() {
        let repo: LoginRepository = LoginRepository(level: CABaseLevel())
        repo.getProfile()
    }
}
