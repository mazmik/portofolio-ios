//
//  PostLoginApiWithoutAlamofire.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation
class PostLoginApiWithoutAlamofire: CABaseRequest {
    var method: CABaseHttpMethod = .POST
    var path: String = "/auth/login"
    var parameters: [String : Any]
    var apiVersion: ApiVersion = .v1
    var shouldWithToken: Bool = false
    
    init(parameters: [String:Any]) {
        self.parameters = parameters
    }
}

struct GetProfileRequest: CABaseRequest {
    var method: CABaseHttpMethod = .GET
    var path: String = "/auth/show/"
    var parameters: [String : Any] = [:]
    var apiVersion: ApiVersion = .none
    var shouldWithToken: Bool = true
}
