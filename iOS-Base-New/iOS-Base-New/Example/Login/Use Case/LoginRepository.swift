//
//  PostRepository.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 16/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

class LoginRepository: BaseRepository {
    
    func onPostLoginNative(body: PostLoginBodyRequest, onSuccess: OnSuccessNative<PostLoginOutput>, onFailed: OnFailed) {
        if let dict = body.dictionary {
            let request = PostLoginApiWithoutAlamofire(parameters: dict)
            let loginRequest: Request<PostLoginBodyResponse> = Request<PostLoginBodyResponse>()
            loginRequest.request(level: level, auth: CAAuth(), request: request) { (response, err) in
                if let response = response {
                    switch response.handle() {
                    case .success(let result):
                        print(result.firstName + " " + result.lastName)
                        let output = PostLoginOutput(
                            customerId: result.customerId,
                            firstName: result.firstName,
                            lastName: result.lastName,
                            email: result.email,
                            phone: result.phone,
                            createdAt: result.createdAt,
                            updatedAt: result.updatedAt
                        )
                        onSuccess?(output)
                    case .failure(let err):
                        onFailed?(err)
                    }
                } else if let err = err  {
                    print(err)
                    onFailed?(err)
                }
            }
        }
    }
    
    func getProfile() {
        let request = GetProfileRequest()
        let loginRequest: Request<GetProfileBodyResponse> = Request<GetProfileBodyResponse>()
        loginRequest.request(level: level, auth: CAAuth(), request: request) { (response, err) in
            if let response = response {
                switch response.handle() {
                case .success(let result):
                    print(result)
                case .failure(let err):
                    print(err)
//                    onFailed?(err)
                }
            } else if let err = err  {
                print(err)
//                onFailed?(err)
            }
        }
    }
}
