//
//  ClaimsResponse.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation
struct ClaimsBodyResponse: CABaseResponse {
    typealias CAOutput = GetClaimsBodyResponse
    var data: GetClaimsBodyResponse?
    var meta: CABaseMeta
}

struct GetClaimsBodyResponse: Codable {
    var subjects: [GetClaimBodyResponse]
}

struct GetClaimBodyResponse: Codable {
    let subjectId: Int
    let subjectName: String
}

struct GetClaimOutput {
    let subjectId: Int
    let subjectName: String
}
