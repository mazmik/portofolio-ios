//
//  ClaimsPresentation.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

class ClaimsPresentation: ClaimsPresenter {
    
    private let view: ComplaintsView!
    
    init(view: ComplaintsView) {
        self.view = view
    }
    
    func getClaims() {
        let repo: ComplaintRepository = ComplaintRepository(level: CABaseLevel())
        view.onLoading(msg: "Loading")
        repo.getClaim(onSuccess: { (results) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                self.view.onFinishLoading()
                self.view.getClaims(claims: results)
            }
        }) { (err) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {return}
                self.view.onFinishLoading()
                self.view.showError(msg: err.localizedDescription)
            }
        }
    }
}
