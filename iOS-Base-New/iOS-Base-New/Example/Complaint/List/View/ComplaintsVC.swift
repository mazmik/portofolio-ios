//
//  ComplaintsVC.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 21/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

class ComplaintsVC: CABaseVC {

    @IBOutlet weak var tableView: UITableView!
    private var complaints: [GetClaimOutput] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let presenter: ClaimsPresenter = ClaimsPresentation(view: self)
        presenter.getClaims()
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
    }

}

extension ComplaintsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let complaint = complaints[indexPath.row]
        cell.textLabel?.text = "\(complaint.subjectId)"
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        cell.detailTextLabel?.text = complaint.subjectName
        return cell
    }
}

extension ComplaintsVC: ComplaintsView {
    func getClaims(claims: [GetClaimOutput]) {
        complaints = claims
        tableView.reloadData()
    }
}
