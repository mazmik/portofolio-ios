//
//  ComplaintRepository.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

class ComplaintRepository: BaseRepository {
    func getClaim(onSuccess: OnSuccessNative<[GetClaimOutput]>, onFailed: OnFailed) {
        let claimsRequest = Request<ClaimsBodyResponse>()
        claimsRequest.request(level: CABaseLevel(), auth: CAAuth(), request: GetComplaintsApi()) { (response, err) in
            if let response = response {
                switch response.handle() {
                case .success(let output):
                    var claims: [GetClaimOutput] = []
                    output.subjects.forEach { (result) in
                        let claim: GetClaimOutput = GetClaimOutput(
                            subjectId: result.subjectId,
                            subjectName: result.subjectName)
                        claims.append(claim)
                    }
                    onSuccess?(claims)
                case .failure(let err):
                    onFailed?(err)
                }
            } else if let err = err  {
                print(err)
                onFailed?(err)
            }
        }
    }
}
