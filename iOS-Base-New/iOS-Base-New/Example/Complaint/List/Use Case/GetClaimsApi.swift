//
//  GetClaimsApi.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

class GetComplaintsApi: CABaseRequest {
    var method: CABaseHttpMethod = .GET
    var path: String = "/subject"
    var parameters: [String : Any] = [String:Any]()
    var apiVersion: ApiVersion = .v1
    var module: Module = .complaint
    var shouldWithToken: Bool = false
}
