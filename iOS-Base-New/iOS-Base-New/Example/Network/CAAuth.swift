//
//  CAAuth.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation
class CAAuth: CABaseAuth {
    var bearerToken: String? {
        guard let token = CAPreference.getString(forKey: .TOKEN_BASE) else {
            return "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjp7IiRvaWQiOiI1ZTMwMDNjOGVkODU2YTAwMDE3M2Q2ZDUifSwiZXhwIjoxNTgwNDAzMTk5fQ.66MFq0byM5X_X3tvTW1wuOKndOTA4sxlUbpaFytiP4I"
        }
        return "Bearer \(token)"
    }
    
    func saveToken(token: String) {
        CAPreference.set(value: token, forKey: .TOKEN_BASE)
    }
    
    func removeToken(token: String) {
        CAPreference.set(value: nil, forKey: .TOKEN_BASE)
    }
}
