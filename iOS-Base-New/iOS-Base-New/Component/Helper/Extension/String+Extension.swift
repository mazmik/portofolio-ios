//
//  String+Extension.swift
//  Storial
//
//  Created by Azmi Muhammad on 28/10/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import Foundation
extension String {
    func isEmpty() -> Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines) == "" || isEmpty
    }
}
