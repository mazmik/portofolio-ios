//
//  UIView+Extension.swift
//  Storial
//
//  Created by Azmi Muhammad on 15/10/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit
enum RadiusType {
    case rounded
    case quarter
    case custom(CGFloat)
}

extension UIView {
    
    fileprivate typealias Action = (() -> Void)?
    
    func setupRadius(type: RadiusType, isMaskToBounds: Bool = false) {
        var radius: CGFloat = 0.0
        
        switch type {
        case .rounded:
            radius = frame.width / 2
        case .quarter:
            radius = frame.width / 4
        case .custom(let value):
            radius = value
        }
        
        layer.cornerRadius = radius
        layer.masksToBounds = isMaskToBounds
    }
    
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    @objc func tapGesture(action: (() -> Void)?) {
        isUserInteractionEnabled = true
        tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    func setupBorder(color: UIColor = .black) {
        layer.borderColor = color.cgColor
        layer.borderWidth = 1
    }
    
    func blurEffect(effect: UIBlurEffect.Style = .light) {
        let blurEffect = UIBlurEffect(style: effect)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        translatesAutoresizingMaskIntoConstraints = false
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(blurEffectView, at: 0)
    }
    
    func addShadow2() {
        layer.shadowOffset = CGSize(width:0, height:0)
        layer.cornerRadius = 5.0
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 1.00
        
        layer.shadowRadius = 4
        layer.shadowColor = UIColor(string: "#656565").cgColor
        layer.shadowOpacity = 0.30
        layer.shadowOffset = CGSize(width:1, height:1)
        layer.masksToBounds = false
    }
    
    func addShadow3() {
        layer.shadowOffset = CGSize(width:0, height:0)
        layer.cornerRadius = 8.0
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 1.00
        layer.borderWidth = 0.2
        layer.borderColor = UIColor(red: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 0.25).cgColor
        
        layer.shadowRadius = 1.5
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.15
        layer.shadowOffset = CGSize(width:1, height:1)
        layer.masksToBounds = false
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, color: UIColor = .white) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.backgroundColor = color.cgColor
        layer.mask = mask
    }
}
