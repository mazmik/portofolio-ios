//
//  UIViewController+Extension.swift
//  Storial
//
//  Created by Azmi Muhammad on 14/10/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

extension UIViewController {
    func setupHideKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController {
    func hideNavigationBar() {
        navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar() {
        navigationController?.isNavigationBarHidden = false
    }
    
    func backToPreviousVC() {
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func backToRootVC() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func pushVC<V: CABaseVC>(_ vc: V) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    
    func present<V: UIViewController>(_ vc: V, isFullScreen: Bool = false, isWithNavController: Bool = false, completion: (()->Void)? = nil) {
        if isWithNavController {
            let navController = UINavigationController(rootViewController: vc)
            if #available(iOS 13.0, *) {
                navController.modalPresentationStyle = isFullScreen ? .fullScreen : .automatic
            }
            present(navController, animated: true, completion: completion)
        } else {
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = isFullScreen ? .fullScreen : .automatic
            }
            present(vc, animated: true, completion: completion)
        }
        
    }
    
    func navItem(icon: String = "", title: String = "", color: UIColor = .black, onTap: (() -> Void)?) -> UIBarButtonItem? {
        if !icon.elementsEqual("") {
            let image = UIImage(named: icon)
            let button = UIButton(type: .custom)
            button.setImage(image, for: .normal)
            button.tapGesture(action: onTap)
            button.setTitle(" \(title)", for: .normal)
            button.setTitleColor(color, for: .normal)
            let item = UIBarButtonItem(customView: button)
            return item
        } else {return nil}
    }
    
    func setupNavBar(title: String, action: (()->Void)? = nil) {
        guard let nav = navigationController else {return}
        nav.navigationBar.backgroundColor = .white
        self.title = title
        let back = navItem(icon: "iconBackBlack", onTap: action ?? backToPreviousVC)
        navigationItem.leftBarButtonItem = back
    }
    
    func setupLargeNavBar(title: String, back: (()->Void)? = nil) {
        guard let nav = navigationController else {return}
        nav.navigationBar.prefersLargeTitles = true
        self.title = title
        let back = navItem(icon: "arrowBackWhite2", onTap: back ?? backToPreviousVC)
        navigationItem.leftBarButtonItem = back
    }
    
    func modifyToSmallNavBar() {
        guard let nav = navigationController else {return}
        nav.navigationBar.prefersLargeTitles = false
        nav.navigationBar.backgroundColor = .clear
    }
    
    @available(iOS 13.0, *)
    func setupDarkModeNavBar(isDarkMode: Bool) {
        guard let navigationController = navigationController, let rightItems = navigationItem.rightBarButtonItems else { return }
        let color: UIColor = isDarkMode ? .white : .black
        let navBar = UINavigationBarAppearance()
        navBar.configureWithOpaqueBackground()
        navBar.backgroundColor = isDarkMode ? .black : .white
        navigationController.navigationBar.isTranslucent = isDarkMode
        navigationController.navigationBar.compactAppearance = navBar
        navigationController.navigationBar.standardAppearance = navBar
        let backButton = navItem(icon: "iconBackBlack", title: "Bab 3", color: color, onTap: dismissVC)
        navigationItem.leftBarButtonItem = backButton
        rightItems.forEach {
            $0.tintColor = color
        }
    }
}
