//
//  UILabel+Extension.swift
//  Storial
//
//  Created by Azmi Muhammad on 15/10/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit
extension UILabel {
    func underlinedText(underlinedWord word: String, color: UIColor = .blue) {
        guard let text = text else {return}
        let attributedString = NSMutableAttributedString(string: text)
        let range = (text as NSString).range(of: word)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : color, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: range)
        attributedText = attributedString
    }
    
    func makeSubstringColorsIndividual(boldedWord word: String, fontColor: String) {
        guard let text = text else {return}
        let attr = attributedText!.mutableCopy() as! NSMutableAttributedString
        let range = (text.lowercased() as NSString).range(of: word.lowercased())
        if range.location != NSNotFound {
            let color = UIColor(string: fontColor)
            attr.setAttributes([NSAttributedString.Key.foregroundColor: color], range: range)
        }
        
        attributedText = attr
    }
}
