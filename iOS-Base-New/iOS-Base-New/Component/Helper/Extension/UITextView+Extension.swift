//
//  UITextView+Extension.swift
//  Storial
//
//  Created by Azmi Muhammad on 19/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit
extension UITextView {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(quoteAction) {
            return true
        } else {
            return false
        }
    }
    
    @objc func quoteAction() {
        if let range = selectedTextRange, let selectedText = text(in: range) {
            print(selectedText)
        }
    }
}
