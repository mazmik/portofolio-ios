//
//  CALogger.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

class CALogger {
    static func printAPI(withRequest request: URLRequest) {
        print("HEADER = \(String(describing: request.allHTTPHeaderFields))")
        print("BODY = \(String(describing: request.httpBody))")
        print("URL = \(String(describing: request.url?.absoluteString))")
        print("METHOD = \(request.httpMethod ?? "NONE")")
    }
}

