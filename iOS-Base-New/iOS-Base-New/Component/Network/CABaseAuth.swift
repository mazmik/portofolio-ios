//
//  CAAuth.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

protocol CABaseAuth {
    func saveToken(token: String)
    func removeToken(token: String)
    var bearerToken: String? {get}
}
