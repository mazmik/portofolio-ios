//
//  CABaseRequest.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

enum ApiVersion: String {
    case none
    case v1
    case v2
}

protocol CABaseRequest {
    var method: CABaseHttpMethod {get}
    var path: String {get}
    var parameters: [String:Any] {get}
    var apiVersion: ApiVersion {get}
    var shouldWithToken: Bool {get}
}

extension CABaseRequest {
    func setupRequest(withUrl baseUrl: URL, withAuth auth: CABaseAuth) -> URLRequest {
        let version = apiVersion == .none ? "" : "/\(apiVersion.rawValue)"
        let url = "\(baseUrl.absoluteString)\(version)\(path)"
        let finalUrl = URL(string: url)!
        
        var request = finalUrl.setParameter(parameters: parameters, method: method)
        if shouldWithToken {
            request.setHeader(auth: auth)
        }
        request.httpMethod = method.rawValue
        CALogger.printAPI(withRequest: request)
        return request
    }
}
