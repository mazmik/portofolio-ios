//
//  HttpMethod.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 21/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation
enum CABaseHttpMethod: String {
    case GET
    case HEAD
    case POST
    case PUT
    case DELETE
    case CONNECT
    case OPTIONS
    case TRACE
    case PATCH
}
