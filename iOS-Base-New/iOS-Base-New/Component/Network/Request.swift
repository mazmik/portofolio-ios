//
//  Request.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 21/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

typealias ResultCompletion<T: Codable> = ((T?, Error?)->Void)?
typealias OnSuccessNative<T> = ((T)->Void)?
typealias OnFailed = ((Error)->Void)?

class Request<T: CABaseResponse> {
    func request(level: ApiLevel, auth: CABaseAuth, request: CABaseRequest, result: ResultCompletion<T>) {
        let callRequest = request.setupRequest(withUrl: level.setupBaseURL(), withAuth: auth)
        let task = URLSession.shared.dataTask(with: callRequest, completionHandler: { (data, _, _) in
            if Reachability.isConnectedToNetwork() {
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let model: T = try decoder.decode(T.self, from: data ?? Data())
                    result?(model, nil)
                } catch let error {
                    result?(nil, error)
                }
            } else {
                let err: Error = NSError(domain: "Gagal terhubung dengan server. Pastikan koneksi Anda bagus", code: 523, userInfo: nil)
                result?(nil, err)
            }
        })
        task.resume()
    }
}

extension URL {
    func setParameter(parameters: [String: Any], method: CABaseHttpMethod) -> URLRequest {
        switch method {
        case .GET:
            var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true)!
            var queryItems: [URLQueryItem] = []
            for key in parameters.keys {
                queryItems.append(URLQueryItem(name: key, value: "\(parameters[key]!)"))
            }
            urlComponents.queryItems = queryItems
            urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            return URLRequest(url: urlComponents.url!)
        default:
            var request = URLRequest(url: self)
            let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
            request.httpBody = jsonData
            return request
        }
    }
}

extension URLRequest {
    mutating func setHeader(auth: CABaseAuth) {
        setAuthHeader(auth: auth)
        addValue("application/json", forHTTPHeaderField: "Content-Type")
        addValue("application/json", forHTTPHeaderField: "accept")
    }
    
    private mutating func setAuthHeader(auth: CABaseAuth) {
        if let bearerToken = auth.bearerToken {
            addValue(bearerToken, forHTTPHeaderField: "Authorization")
        } else {
            addValue("", forHTTPHeaderField: "Authorization")
        }
    }
}
