//
//  CABaseResponse.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation

protocol CABaseResponse: Codable {
    associatedtype CAOutput
    var data: CAOutput? {get set}
    var meta: CABaseMeta {get set}
}

struct CABaseMeta: Codable {
    let code: Int
    let status: Bool
    let message: String
}

enum CAMetaCode: Int {
    case verificationRequired = 100
    case success = 200
    case created = 201
    case accessTokenExpired = 401
    case notfound = 404
    case sessionExpired = 418
    case serviceUnavailable = 503
    case connectionLost = 0
    case none
}

extension CABaseResponse {
    func handle() -> Result<CAOutput, Error> {
        if meta.status {
            if meta.code == CAMetaCode.success.rawValue || meta.code == CAMetaCode.created.rawValue {
                if let data = data {return .success(data)}
                else {return .failure(NSError(domain: "Object mapping failed", code: 400, userInfo: nil))}
            } else {return .failure(NSError(domain: meta.message, code: meta.code, userInfo: nil))}
        } else {
            return .failure(NSError(domain: meta.message, code: meta.code, userInfo: nil))
        }
    }
}
