//
//  CABaseLevel.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 22/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

protocol ApiLevel {
    var dev: String {get}
    var prod: String {get}
}

extension ApiLevel {
    func setupBaseURL() -> URL {
        #if DEBUG
            return URL(string: dev)!
        #else
            return URL(string: prod)!
        #endif
    }
}
