//
//  BaseVC.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

import UIKit

class CABaseVC: UIViewController {
    private let loadingView: UIView = UIView()
    private var loadView: Loading!
}

extension CABaseVC: CABaseView {
    func showError(msg: String) {
        let alert = UIAlertController(title: "An Error Occured", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true)
    }
    
    func onLoading(msg: String) {
        showLoading(msg: msg)
    }
    
    private func showLoading(msg: String) {
        loadView = Loading(text: msg)
        loadingView.frame = view.frame
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        view.addSubview(loadView)
        loadView.show()
    }

    func onFinishLoading() {
        hideLoading()
    }
    
    private func hideLoading() {
        loadView.hide()
        loadView.removeFromSuperview()
    }
}
