//
//  BaseView.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 28/12/19.
//  Copyright © 2019 Clapping Ape. All rights reserved.
//

protocol CABaseView {
    func showError(msg: String)
    func onLoading(msg: String)
    func onFinishLoading()
}
