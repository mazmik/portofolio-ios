//
//  BaseRepository.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 16/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import UIKit

class BaseRepository: Repository {
    
    let parsingError = NSError(domain: "Gagal mendapatkan data dari server. Coba lagi", code: 400, userInfo: nil)
    let level: ApiLevel!
    
    required init(level: ApiLevel) {
        self.level = level
    }
}
