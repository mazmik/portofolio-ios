//
//  Repository.swift
//  iOS-Base-New
//
//  Created by Azmi Muhammad on 16/01/20.
//  Copyright © 2020 Clapping Ape. All rights reserved.
//

import Foundation
protocol Repository {
    init(level: ApiLevel)
}
