//
//  PDBillerPulsaProviderAPI.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDBillerPulsaProviderAPI {
    
    private class GetPulsaProviderRequest: HTTPRequest {
        var method = HTTPMethods.POST
        var path = "/getHpDenomListPost"
        var apiVersion = ApiVersion.none
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.basic
        var header = HeaderType.biller
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDBillerPulsaProviderResponse> {
        return httpClient.send(request: GetPulsaProviderRequest(parameters: parameters))
    }
}
