//
//  PDPulsaView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 04/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDPulsaView: BaseView {
    var onInquiry: ((BillerInquiryItem) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
}
