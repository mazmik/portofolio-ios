//
//  PDBillerLoadingVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDBillerLoadingVM {
    
    private let repository: PDBillerLoadingRepository
    private let disposeBag = DisposeBag()
    
    init(repository: PDBillerLoadingRepository) {
        self.repository = repository
    }
    
    func requestBillingInquiry(pin: String, model: PDBillerInquiryModel) -> PublishSubject<PDBillerLoadingModelOutputFullInfo> {
        let requestModel = PDBillerLoadingModelInput(inquiry: model, pin: pin)
        return requestAPI(model: requestModel)
    }
    
    func disableSmallIntro() -> Single<Bool> {
        let data = PublishSubject<Bool>()
        
        self.repository.disableSmallIntro()
            .subscribe { (event) in
                switch event {
                case .error(let err):
                    data.onError(err)
                case .completed:
                    data.onNext(true)
                    data.onCompleted()
                }
            }
            .disposed(by: disposeBag)
        
        return data.asSingle()
    }
    
    private func requestAPI(model: PDBillerLoadingModelInput) -> PublishSubject<PDBillerLoadingModelOutputFullInfo> {
        let data = PublishSubject<PDBillerLoadingModelOutputFullInfo>()
        
        self.repository.requestAPI(model: model)
            .subscribe { (event) in
                switch event {
                case .error(let err):
                    data.onError(err)
                case .success(let model):
                    data.onNext(model)
                    data.onCompleted()
                }
            }
            .disposed(by: disposeBag)
        
        return data
    }
}
