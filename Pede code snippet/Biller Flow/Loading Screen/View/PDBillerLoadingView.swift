//
//  PDBillerLoadingView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerLoadingView: BaseView {
    var onExpired: (() -> Void)? { get set }
    var onSuccess: ((PDBillerInquiryModel, PDBillerLoadingModelOutputFullInfo) -> Void)? { get set }
    var onCanceled: (() -> Void)? { get set }
    var onBack: (() -> Void)? { get set }
    var onInvestmentTapped: (() -> Void)? { get set }
    var pin: String! { get set }
    var model: PDBillerLoadingModelOutputFullInfo! {get set}
    var inqury: PDBillerInquiryModel! { get set }
}
