//
//  PDBillerLoadingVC+Ext.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 20/06/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import RxSwift

extension PDBillerLoadingVC {
    @objc func tapcpopCloseBaseMe() {
        popUpView.removePopup { (isfinished) in
            self.onSuccess?(self.inqury, self.model)
        }
    }
    
    @objc func tapcIurSelengkapnya() {
        self.onInvestmentTapped?()
    }
    
    @objc func tapcIurJanganTampilkan() {
        popUpView.removePopup { (isfinished) in
            self.viewModel.disableSmallIntro()
                .subscribeOn(SchedulerProvider.shared.background)
                .observeOn(SchedulerProvider.shared.main)
                .subscribe { [weak self] (event) in
                    guard let self = self else { return }
                    self.outputHandler(event: event)
                }
                .disposed(by: self.disposeBag)
        }
    }
    
    private func outputHandler(event: SingleEvent<Bool>) {
        switch event {
        case .success(let value):
            if value {
                self.closeButton.isHidden = false
                self.onSuccess?(self.inqury, self.model)
            } else {
                let emoji = Emojis.EmojiFailed.sm_random()!
                self.showCardWithEmoji(  MessageStatus.Error, emoji: emoji, title: "Warning", body: "Permintaan Anda Gagal", buttonTitle: nil)
            }
        case .error(let err):
            self.errorHandler(err: err)
        }
    }
    
    private func setupConfig() {
        popUpConfig.dimBackground = true
        popUpConfig.animationType = .scale
        popUpConfig.shadowEnabled = true
        popUpConfig.blurBackground = true
        popUpConfig.autoDismiss = true
        popUpConfig.cornerRadius =  4
        popUpConfig.blurRadius =  8
        popUpConfig.animaionDuration = Double(0.4)
        popUpConfig.blurTrackingMode = .none
    }
    
    func initMessageIUR() {
        setupConfig()
        popUpView.config = popUpConfig
        let tappop2 =  UITapGestureRecognizer(target: self, action: #selector(self.tapcIurSelengkapnya))
        
        popviewJG.bt_selengkapnya.addGestureRecognizer(tappop2)
        popviewJG.bt_selengkapnya.isUserInteractionEnabled = true
        
        let tappop1 =  UITapGestureRecognizer(target: self, action: #selector(self.tapcIurJanganTampilkan))
        
        popviewJG.bt_jangan_tampilkan.addGestureRecognizer(tappop1)
        popviewJG.bt_jangan_tampilkan.isUserInteractionEnabled = true
        popviewJG.bt_jangan_tampilkan.addShadow()
        
        let tappop3 =  UITapGestureRecognizer(target: self, action: #selector(self.tapcpopCloseBaseMe))
        
        popviewJG.bt_close.addGestureRecognizer(tappop3)
        popviewJG.bt_close.isUserInteractionEnabled = true
        popUpView.config = popUpConfig
        
        if self.model.smallChangeIntro {
            popUpView.showPopup()
        } else {
            self.onSuccess?(self.inqury, self.model)
        }
    }
}
