//
//  PDBillerLoadingVC.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EasyPopUp
import RxSwift
import RxCocoa

class PDBillerLoadingVC: UIViewController, PDBillerLoadingView {
    var onSuccess: ((PDBillerInquiryModel, PDBillerLoadingModelOutputFullInfo) -> Void)?
    var onCanceled: (() -> Void)?
    var onBack: (() -> Void)?
    var onInvestmentTapped: (() -> Void)?
    var pin: String!
    var model: PDBillerLoadingModelOutputFullInfo!
    var inqury: PDBillerInquiryModel!
    var viewModel: PDBillerLoadingVM!
    var onExpired: (() -> Void)?
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var loadingCircleView: UIView!
    @IBOutlet weak var loadingBarView: UIView!
    @IBOutlet weak var loadingBaseImage: UIImageView!
    
    @IBOutlet weak var loadingMessageLabel: UILabel!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var ribbonImageView: UIImageView!
    
    @IBOutlet weak var backButton: BaseButtonPrimary!
    @IBOutlet weak var viewBackButton: UIView!
    
    var loadingCircle = NVActivityIndicatorView(frame: CGRect.zero)
    var loadingEqualizer = NVActivityIndicatorView(frame: CGRect.zero)
    let disposeBag = DisposeBag()
    
    let popviewJG = PopIUR().loadNib() as! PopIUR
    var popUpConfig = EasyPopupConfig()
    lazy var popUpView: EasyPopup = {
        let easePopUp = EasyPopup(superView: self.view, viewTopop: popviewJG)
        return easePopUp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        noteView.isHidden = true
        self.setupDisplay()
        self.setupLoadingCircleView()
        self.view.backgroundColor = UIColor(red: 25, green: 25, blue: 59)
        self.initTransactionView()
        self.requestApi()
        self.closeButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func requestApi() {
        let result = viewModel.requestBillingInquiry(pin: pin, model: inqury)
        
        result
            .subscribeOn(SchedulerProvider.shared.background)
            .observeOn(SchedulerProvider.shared.main)
            .subscribe { [weak self] (event) in
                guard let self = self else { return }
                self.outputHandler(event: event)
            }
            .disposed(by: disposeBag)
    }
    
    func setupDisplay() {
        self.viewBackButton.isHidden = true
        self.setupLoadingCircleView()
        self.setuploadingBarView()
    }
    
    func setupLoadingCircleView(){
        DispatchQueue.main.async() {
            let frame = CGRectMake(0, 0,  self.loadingCircleView.frame.size.width, self.loadingCircleView.frame.size.height)
            self.loadingCircle = NVActivityIndicatorView(frame: frame,
                                                         type: NVActivityIndicatorType.ballScaleMultiple, color: UIColor(string: colorPrimaryCode).withAlphaComponent(0.3))
            self.loadingCircleView.insertSubview(self.loadingCircle, at: 0)
            self.loadingCircle.startAnimating()
        }
    }
    
    func setuploadingBarView(){
        let frame = CGRectMake(0, 0,  self.loadingBarView.frame.size.width, self.loadingBarView.frame.size.height)
        loadingEqualizer = NVActivityIndicatorView(frame: frame,
                                                   type: NVActivityIndicatorType.audioEqualizer, color: UIColor(string: colorPrimaryCode).withAlphaComponent(0.3))
        self.loadingBarView.addSubview(loadingEqualizer)
        self.loadingBarView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        loadingEqualizer.startAnimating()
    }
    
    private func outputHandler(event: Event<PDBillerLoadingModelOutputFullInfo>) {
        switch event {
        case .next(let model):
            self.model = model
            if inqury.smallChangeSubscribed {
                self.onSuccess?(self.inqury, self.model)
            } else {
                initMessageIUR()
            }
        case .error(let err):
            self.errorHandler(err: err)
        case .completed:
            self.loadingFinish()
        }
    }
    
    func loadingFinish() {
        stopAnimation()
        let note = "(Termasuk biaya IUR \(PDFormatter.currency(number: self.inqury.smallChange.amount)))"
        initTransactionView(icon: "ic_success_teal", title: "TOTAL PEMBAYARAN", amount: PDFormatter.currency(number: inqury.transactionAmount), note: note, msg: "Transaksi Berhasil")
        ribbonImageView.isHidden = false
        noteView.isHidden = false
    }
    
    func setupFailedTransaction(msg: String) {
        stopAnimation()
        initTransactionView(icon: "circle_failed", title: msg, amount: "", note: "", msg: "Transaksi gagal", isFailed: true)
        self.viewBackButton.isHidden = false
        noteView.isHidden = false
    }
    
    func initTransactionView(icon: String = "pede-navigation-logo", title: String = "", amount: String = "", note: String = "", msg: String = "Memproses Transaksi", isFailed: Bool = false) {
        loadingBaseImage.image = UIImage(named: icon)
        titleLabel.text = title
        amountLabel.text = amount
        noteLabel.text = note
        loadingMessageLabel.text = msg.capitalized
        
        if !inqury.smallChangeSubscribed || self.inqury.smallChange.amount == 0 {
            noteLabel.isHidden = true
        }
        
        if isFailed {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
            titleLabel.textColor = .red
        }
    }
    
    func stopAnimation() {
        loadingCircle.stopAnimating()
        loadingEqualizer.stopAnimating()
    }
    
    func errorHandler(err: Error) {
        PDErrorHandler.shared.isCommon(error: err, onExpired: { [weak self] in
            guard let self = self else { return }
            self.onExpired?()
        }, onLost: { [weak self] (warning) in
            guard let self = self else { return }
            self.setupFailedTransaction(msg: warning)
            self.popupShowError(message: warning)
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.setupFailedTransaction(msg: error.description)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.onBack?()
    }
    
    @IBAction func onCancelledButton(_ sender: UIButton) {
        self.onCanceled?()
    }
}
