//
//  PDBillerDisableSmallIntroModel.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 23/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import Foundation

struct PDBillerDisableSmallIntroModelOutput: PDModelOutput {
    typealias output = PDBillerDisableSmallIntroModelOutputFullInfo
    var meta: PDMeta
    var data: PDBillerDisableSmallIntroModelOutputFullInfo?
}

struct PDBillerDisableSmallIntroModelOutputFullInfo {
    
}
