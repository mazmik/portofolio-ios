//
//  PDBillerDisableSmallIntroResponse.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 23/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import Foundation

struct PDBillerDisableSmallIntroResponse: Codable {
    let code: Int
    let status: String
    let message: String
}
