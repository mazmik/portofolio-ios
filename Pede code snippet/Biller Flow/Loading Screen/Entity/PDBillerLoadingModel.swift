//
//  PDBillerLoadingModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDBillerLoadingModelInput {
    let inquiry: PDBillerInquiryModel
    let pin: String
}

struct PDBillerLoadingModelOutput: PDModelOutput {
    typealias output = PDBillerLoadingModelOutputFullInfo
    var meta: PDMeta
    var data: PDBillerLoadingModelOutputFullInfo?
}

struct PDBillerLoadingModelOutputFullInfo {
    let inquiryId: Int
    let smallChangeIncluded: Bool
    let smallChangeAmount: Int
    let transactionAmount: Int
    let ottocashRef: String
    let smallChangeIntro: Bool
    let date: String
    let tokenNumber: String?
}
