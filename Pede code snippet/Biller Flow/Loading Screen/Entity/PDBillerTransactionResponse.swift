//
//  PDBillerTransactionResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//{
//    "inquiryId": 2,
//    "pin": "123456",
//    "smallChangeIncluded": true,
//    "smallChangeAmount": 1500,
//    "transactionAmount": 12500
//}

struct PDBillerTransactionBody: Codable {
    let inquiryId: Int
    let pin: String
    let smallChangeIncluded: Bool
    let smallChangeAmount: Int
    let transactionAmount: Int
}

//{
//    "code": 200,
//    "status": "success",
//    "message": "",
//    "data": {
//        "inquiryId": 3156,
//        "smallChangeIncluded": true,
//        "smallChangeAmount": 1500,
//        "transactionAmount": 12500,
//        "ottocashRef": "8A0500000339",
//        "smallChangeIntro": true,
//        "date": "05/11/2018 13:37:13"
//    }
//}

struct PDBillerTransactionResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDBillerTransactionResponseFullInfo?
}

struct PDBillerTransactionResponseFullInfo: Codable {
    let inquiryId: Int
    let smallChangeIncluded: Bool
    let smallChangeAmount: Int
    let transactionAmount: Int
    let ottocashRef: String
    let smallChangeIntro: Bool
    let date: String
    let tokenNumber: String?
}
