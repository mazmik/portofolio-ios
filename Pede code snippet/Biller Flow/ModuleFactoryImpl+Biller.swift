//
//  ModuleFactoryImpl+Biller.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 09/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

extension ModuleFactoryImpl: PDBillerModuleFactory {
    func makePulsaView() -> PDPulsaView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedePulsa1NewVC") as! PedePulsa1NewVC
        return vc
    }
    
    func makeTALView() -> PDTALView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedeTrxTelAir1Vc") as! PedeTrxTelAir1VC
        return vc
    }
    
    func makeTVKabelView() -> PDTVKabelView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedeTrxTelAir1Vc") as! PedeTrxTelAir1VC
        return vc
    }
    
    func makeBPJSView() -> PDBPJSView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "BpjsVC") as! BpjsVC
        return vc
    }
    
    func makePrepaidView() -> PDPrepaidView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedeTrxPLN1NewVC") as! PedeTrxPLN1NewVC
        return vc
    }
    
    func makePostpaidView() -> PDPostpaidView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedeTrxTelAir2Vc") as! PedeTrxTelAir2VC
        return vc
    }
    
    func makePostpaidConfirmationView() -> PDPostpaidConfirmationView {
        let vc = UIStoryboard(name:"StrPedeTrx", bundle:nil).instantiateViewController(withIdentifier: "PedeTrxTelAir3Vc") as! PedeTrxTelAir3VC
        return vc
    }
    
    func makeInquiryView() -> PDBillerInquiryView {
        let vc = PDBillerInquiryVC()
        vc.viewModel = makeBillerInquiryVM()
        return vc
    }
    
    func makeSmallChangeView() -> PDBillerSmallChangeView {
        let vc = UIStoryboard(name: "IUR", bundle: nil).instantiateViewController(withIdentifier: "IurUbah1VC") as! IurUbah1VC
        return vc
    }
    
    func makePinView() -> PDBillerPinView {
        let vc = PDBillerPinVC()
        return vc
    }
    
    func makeLoadingView() -> PDBillerLoadingView {
        let vc = PDBillerLoadingVC()
        vc.viewModel = makeBillerLoadingVM()
        return vc
    }
    
    func makeReceiptView() -> PDBillerReceiptView {
        let vc = PDBillerReceiptVC()
        return vc
    }
    
    func makeGameVoucherView() -> PDGameVoucherView {
        let vc = PDGameVoucherVC()
        return vc
    }
}
