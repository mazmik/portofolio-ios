//
//  PDPostpaidConfirmationView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 05/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDPostpaidConfirmationView: BaseView {
    var onInquiry: ((BillerInquiryItem) -> Void)? { get set }
    var stringPss : String? { get set }
    var idTrx : Int? { get set }
    var customerRef: String? { get set }
    var responseme : NSDictionary? { get set }
}
