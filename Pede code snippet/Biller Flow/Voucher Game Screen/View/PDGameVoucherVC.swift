//
//  PDGameVoucherVC.swift
//  Pedev20
//
//  Created by clappingApe on 16/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import WebKit

class PDGameVoucherVC: UIViewController, PDGameVoucherView, WKScriptMessageHandler, WKNavigationDelegate, UIGestureRecognizerDelegate {
    
    var onExit: (() -> Void)?
    var goToInquiry: ((BillerInquiryItem) -> Void)?
    
    var webView: WKWebView!
    @IBOutlet weak var webViewContainer: UIView!
    var tap: UITapGestureRecognizer = UITapGestureRecognizer()
    var lastPageURL = ""
    let BASE_URL = "https://dev.pede.id/web-voucher/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupWKWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.webViewContainer.layoutIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.delegate = self
    }
    
    //MARK: SETUP NAVIGATION
    func setupBackButton(_ selector: Selector? = #selector(webViewBack)){
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back-1"), style: .plain, target: self, action: selector)
    }
    
    @objc func webViewBack() {
        self.webView.evaluateJavaScript("javascript: backToDashboard()", completionHandler: nil)
    }
    
    func hideHistoryNavigation(){
        self.navigationItem.rightBarButtonItem = nil
    }
    
    //MARK: WEBVIEW SETUP
    private func setupWKWebView() {
        
        self.webView = WKWebView(frame: .zero, configuration: self.configuration())
        self.webView.navigationDelegate = self
        
        self.webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        
        if let url = URL(string: BASE_URL) {
            let request = URLRequest(url: url)
            self.webView.load(request)
        }
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.webViewContainer.addSubview(self.webView)
        self.setupWebViewFrame()
    }
    
    func setupWebViewFrame(){
        let margins = webViewContainer.layoutMarginsGuide
        webView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0).isActive = true
        webView.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: 0).isActive = true
        webView.layoutIfNeeded()
    }
    
    func configuration() -> WKWebViewConfiguration{
        let contentController = WKUserContentController()
        
        let userScript = WKUserScript(
            source: self.decompile(contents: self.authScript()),
            injectionTime: WKUserScriptInjectionTime.atDocumentStart,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(self, name: "setTitleFromJs")
        contentController.add(self, name: "checkout")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        return config
    }
    
    func decompile(contents: [JavaScriptModel]) -> String {
        var results = [String]()
        contents.forEach { (content) in
            results.append(content.javascriptMode)
        }
//        let script = "javascript:var TOKEN = 'Bearer \(accessToken ?? "")';" + "javascript:var MEMBERID = 'PEDE';"
        return results.joined()
    }
    
    func authScript() -> [JavaScriptModel] {
        let accessToken = Preference.getString(forKey: .kUserDefaultAccesToken)
        
        let scp = JavaScriptModel(key: "TOKEN", value: "Bearer \(accessToken ?? "")")
        let scp2 = JavaScriptModel(key: "MEMBERID", value: "PEDE")
        
        let script = [scp, scp2]
        
        print("scrip anda : ", script)
        
        return script
    }
    
    // MARK: - WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "setTitleFromJs" {
            self.setTitleFromJs(body: "\(message.body)")
        }
        
        if message.name == "checkout" {
            self.checkout(body: "\(message.body)")
        }
    }
    
    //MARK: JS METHOD
    func setTitleFromJs(body: String){
        let jsonObject = body.parseJSONString
        self.setNavigationTitleWithSubtitle(title: jsonObject?["title"] as? String ?? "", "")
    }
    
    func checkout(body: String) {
        let jsonObject = body.parseJSONString
        let data = jsonObject?["data"] as? Dictionary<String, Any>
        let productName = data?["productName"] as? String ?? ""
        
        let item = BillerInquiryItem(inTransaksi: 19, strTransaksi: "Game Voucher", strhead: "Voucher Game", strdesc: "Desc", strnohp: productName, responseme: jsonObject as! NSDictionary, merchantQROffline: "", strCreditName: "Pembayaran Voucher Game")
        
        self.goToInquiry?(item)
    }
    
    //MARK: Detect URL change
    override func observeValue(
        forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey: Any]?,
        context: UnsafeMutableRawPointer?) {
        
        if object as AnyObject? === webView && keyPath == "URL" {
            self.setBackButtonBaseOnURLNavigation(url: webView.url?.absoluteString ?? "")
            
            self.urlChangeAction()
            let parsed = (webView.url?.absoluteString ?? "").replacingOccurrences(of: BASE_URL, with: "")
            self.lastPageURL = parsed
        }
    }
    
    func urlChangeAction() {
        self.clearCache()
        self.hideHistoryNavigation()
    }
    
    func clearCache() {
        if #available(iOS 9.0, *) {
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
            let date = NSDate(timeIntervalSince1970: 0)
            WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler:{ })
        } else {
            var libraryPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, false).first!
            libraryPath += "/Cookies"
            
            do {
                try FileManager.default.removeItem(atPath: libraryPath)
            } catch {
                print("error")
            }
            URLCache.shared.removeAllCachedResponses()
        }
    }
    
    func setBackButtonBaseOnURLNavigation(url: String) {
        let parsed = url.replacingOccurrences(of: BASE_URL, with: "")
        print("url : ", url)
        print("parsed : ", parsed)
        
        if parsed == "" {
            self.setNavigationTitleWithSubtitle(title: "Game Voucher", "")
            self.setupBackButton(#selector(self.exit))
        } else {
            self.setupBackButton(#selector(self.listGameVoucher))
        }
    }
    
    @objc func exit(){
        self.clearWebView()
        self.onExit?()
    }
    
    @objc func listGameVoucher() {
        self.clearWebView()
        self.setupWKWebView()
    }
    
    func clearWebView() {
        self.webView.scrollView.removeGestureRecognizer(self.tap)
        self.webView = nil
    }
    
    func setNavigationTitleWithSubtitle(title: String,_ subtitle: String = ""){
        let navLabel = UILabel()
        navLabel.numberOfLines = 2
        navLabel.textAlignment = .center
        let navTitle = NSMutableAttributedString(string: title, attributes:[
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: UIFont(name: fontFamilyNormalSemiBold, size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)])
        
        if subtitle != ""{
            navTitle.append(NSMutableAttributedString(string: "\n\(subtitle)", attributes:[
                NSAttributedStringKey.font: UIFont(name: fontFamilyNormal, size: 10.0) ?? UIFont.systemFont(ofSize: 10.0),
                NSAttributedStringKey.foregroundColor: UIColor.white]))
            
            
        }
        
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func boolToString(value: Bool?) -> String {
        if let value = value {
            return "\(value)"
        }
        else {
            return "false"
            // or you may return nil here. The return type would have to be String? in that case.
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillAppear(notification: NSNotification){
        self.webView.scrollView.addGestureRecognizer(self.tap)
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification){
        self.webView.scrollView.removeGestureRecognizer(self.tap)
    }
}

extension Int {
    var boolValue: Bool { return self != 0 }
}

extension String
{
    var parseJSONString: AnyObject?
    {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data
        {
            // Will return an object or nil if JSON decoding fails
            do
            {
                let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                
                if let jsonResult = message as? NSDictionary
                {
                    return jsonResult //Will return the json object output
                }
                else if let jsonResult = message as? NSArray
                {
                    return jsonResult //Will return the json array output
                }
                else
                {
                    return nil
                }
            }
            catch let error as NSError
            {
                print("An error occurred: \(error)")
                return nil
            }
        }
        else {
            return nil
        }
    }
}
