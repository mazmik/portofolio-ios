//
//  PDGameVoucherView.swift
//  Pedev20
//
//  Created by clappingApe on 15/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDGameVoucherView: BaseView {
    var onExit: (() -> Void)? { get set }
    var goToInquiry: ((BillerInquiryItem) -> Void)? { get set }
}
