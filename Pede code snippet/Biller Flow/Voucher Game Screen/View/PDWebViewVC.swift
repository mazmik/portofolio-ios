//
//  PDWebViewVC.swift
//  Pedev20
//
//  Created by clappingApe on 16/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import WebKit

class PDWebViewVC: UIViewController, PDGameVoucherView {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUrl()
        setupNavigation()
    }
    
    private func setupUrl() {
        let request = URLRequest(url: URL(string: "https://dev.pede.id/web-voucher/")!)
        webView?.load(request)
    }
    
    private func setupNavigation() {
        setTitleNavigationBar(title: "Voucher Game")
//        setupCloseButtonInNavbar()
        //        setupCloseButtonInNavbar(selector: #selector(closeButtonTapped))
    }
}
