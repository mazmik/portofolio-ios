//
//  JavaScriptModel.swift
//  Pedev20
//
//  Created by clappingApe on 22/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct JavaScriptModel {
    let key: String
    let value: String
    
    var javascriptMode: String {
        return "javascript:var \(self.key) = '\(self.value)';"
    }
}
