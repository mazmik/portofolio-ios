//
//  PDTALView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 05/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTALView: BaseView {
    var onProviderTapped: ((BillerType) -> Void)? { get set }
    var intTrans : Int! { get set }
}
