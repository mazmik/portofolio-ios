//
//  PDBillerModuleFactory.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 04/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerModuleFactory {
    func makePulsaView() -> PDPulsaView
    func makeTALView() -> PDTALView
    func makeTVKabelView() -> PDTVKabelView
    func makeBPJSView() -> PDBPJSView
    func makeGameVoucherView() -> PDGameVoucherView
    func makePostpaidView() -> PDPostpaidView
    func makePrepaidView() -> PDPrepaidView
    func makePostpaidConfirmationView() -> PDPostpaidConfirmationView
    func makeInquiryView() -> PDBillerInquiryView
    func makeSmallChangeView() -> PDBillerSmallChangeView
    func makePinView() -> PDBillerPinView
    func makeLoadingView() -> PDBillerLoadingView
    func makeReceiptView() -> PDBillerReceiptView
}
