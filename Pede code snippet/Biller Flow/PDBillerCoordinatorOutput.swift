//
//  PDBillerCoordinatorOutput.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 04/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerCoordinatorOutput {
    var finishFlow: (() -> Void)? { get set }
    var expiredFlow: (() -> Void)? { get set }
    var investmentFlow: (() -> Void)? { get set }
}
