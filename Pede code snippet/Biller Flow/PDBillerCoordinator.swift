//
//  PDBillerCoordinator.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 04/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDBillerCoordinator: BaseCoordinator, PDBillerCoordinatorOutput {
    
    var investmentFlow: (() -> Void)?
    var finishFlow: (() -> Void)?
    var expiredFlow: (() -> Void)?
    
    private let router: Router
    private let factory: PDBillerModuleFactory
    
    init(router: Router, factory: PDBillerModuleFactory) {
        self.router = router
        self.factory = factory
    }
    
    override func start(with productType: PDGroupedProductType) {
        switch productType {
        case .phone:
            showPulsaScreen()
        case .telkomWaterElectric:
            showTelkomAirListrikScreen()
        case .bpjs:
            showBPJSScreen()
        case .tv:
            showTVKabelScreen()
        case .gameVoucher:
            showGameVoucherScreen()
        default:
            print("default")
        }
    }
    
    private func showPulsaScreen() {
        let view = factory.makePulsaView()
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.setRootModule(view, hideBar: true)
    }
    
    private func showTelkomAirListrikScreen() {
        let view = factory.makeTALView()
        view.intTrans = 0
        view.onProviderTapped = { [weak self] (billerType) in
            guard let self = self else { return }
            switch billerType {
            case .postpaid(let text):
                self.showPostpaidScreen(intTrans: 0, text: text)
            case .prepaid(let text):
                self.showPrepaidScreen(text: text)
            }
        }
        router.setRootModule(view, hideBar: true)
    }
    
    private func showTVKabelScreen() {
        let view = factory.makeTVKabelView()
        view.intTrans = 10
        view.onProviderTapped = { [weak self] (billerType) in
            guard let self = self else { return }
            switch billerType {
            case .postpaid(let text):
                self.showPostpaidScreen(intTrans: 10, text: text)
            default:
                print("no action")
            }
        }
        router.setRootModule(view, hideBar: true)
    }
    
    private func showBPJSScreen() {
        let view = factory.makeBPJSView()
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.setRootModule(view, hideBar: true)
    }
    
    private func showGameVoucherScreen() {
        let view = factory.makeGameVoucherView()
        view.onExit = { [weak self] in
            guard let self = self else { return }
            self.finishFlow?()
        }
        view.goToInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        router.setRootModule(view)
    }
    
    private func showPostpaidScreen(intTrans: Int, text: String) {
        let view = factory.makePostpaidView()
        view.stringPss = text
        view.idTrx = intTrans
        view.onVerified = { [weak self] (response, string) in
            guard let self = self else { return }
            self.showPostpaidConfirmationScreen(intTrans: intTrans, text: text, customerRef: string, response: response)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.push(view)
    }
    
    private func showPostpaidConfirmationScreen(intTrans: Int, text: String, customerRef: String, response: NSDictionary) {
        let view = factory.makePostpaidConfirmationView()
        view.stringPss = text
        view.idTrx = intTrans
        view.responseme = response
        view.customerRef = customerRef
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        router.push(view)
    }
    
    private func showPrepaidScreen(text: String) {
        let view = factory.makePrepaidView()
        view.stringPss = text
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.push(view)
    }
    
    private func showInquiryScreen(item: BillerInquiryItem) {
        let creditName = getCreditName(item)
        let view = factory.makeInquiryView()
        view.model = item
        view.onPayTapped = { [weak self] (inquiry, desc) in
            guard let self = self else { return }
            self.showPinScreen(inquiry: inquiry, desc: desc, creditName: creditName.elementsEqual("") ? "Harga Pembelian" : creditName)
        }
        view.onSmallChangeTapped = { [weak self] (price) in
            guard let self = self else { return }
            self.showSmallChangeScreen(price)
        }
        router.push(view)
    }
    
    private func getCreditName(_ item: BillerInquiryItem) -> String {
        guard let data = item.responseme?["data"] as? NSDictionary else { return "" }
        var creditName = ""
        if item.inTransaksi != 1 && item.inTransaksi != 6 {
            if item.inTransaksi == 5 {
                creditName = "Beli "
            }
            creditName += item.strhead ?? ""
        } else if item.inTransaksi == 1 {
            if let strCreditName = item.strCreditName {
                let provider = data["productName"] ?? ""
                let total = PDFormatter.currency(type: .none, withSpacing: false, number: Int(truncating: data["originalPrice"] as! NSNumber))
                if  strCreditName.contains(find: "data") {
                    creditName = "Beli \(provider) Data Pulsa \(total)"
                } else {
                    creditName = "Beli \(provider) Pulsa \(total)"
                }
            } else {
                creditName = ""
            }
        } else if item.inTransaksi == 6 {
            creditName = "Pembayaran Tagihan PLN"
        }
        
        return creditName
    }
    
    private func showSmallChangeScreen(_ price: Int) {
        let view = factory.makeSmallChangeView()
        view.onChanged = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
        }
        view.price = price
        router.push(view)
    }
    
    private func showPinScreen(inquiry: PDBillerInquiryModel, desc: String, creditName: String) {
        let view = factory.makePinView()
        view.desc = desc
        view.onSuccess = { [weak self] (pin, desc) in
            guard let self = self else { return }
            self.router.dismissModule()
            self.showLoadingScreen(pin: pin, inquiry: inquiry, desc: desc, creditName: creditName)
        }
        view.onCancel = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
        }
        router.present(view, animated: true, isWrapNavigation: true)
    }
    
    private func showLoadingScreen(pin: String, inquiry: PDBillerInquiryModel, desc: String, creditName: String) {
        let view = factory.makeLoadingView()
        view.pin = pin
        view.inqury = inquiry
        view.onCanceled = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
        }
        view.onInvestmentTapped = { [weak self] in
            guard let self = self else { return }
            self.investmentFlow?()
        }
        view.onSuccess = { [weak self] (model, result) in
            guard let self = self else { return }
            self.showReceiptScreen(model, result, desc, creditName)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        view.onBack = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
        }
        router.push(view)
    }
    
    private func showReceiptScreen(_ inquiry: PDBillerInquiryModel, _ result: PDBillerLoadingModelOutputFullInfo, _ desc: String, _ creditName: String) {
        let view = factory.makeReceiptView()
        view.desc = desc
        view.descriptionText = creditName
        view.inqury = inquiry
        view.result = result
        view.onCloseTapped = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
            self.finishFlow?()
        }
        router.popover(view)
    }
}
