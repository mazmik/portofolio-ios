//
//  PDBillerSmallChangeRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDBillerSmallChangeRepository {
    
    init() {
        
    }
    
    func smallChangeTransformer(model: PDBillerSmallChangeModel, amount: Int) -> PDBillerSmallChangeModel {
        var smallChange = model
        smallChange.amount = amount
        return smallChange
    }
}
