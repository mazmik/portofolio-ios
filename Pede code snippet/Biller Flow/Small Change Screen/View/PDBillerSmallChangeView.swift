//
//  PDBillerSmallChangeView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerSmallChangeView: BaseView {
    var onChanged: (() -> Void)? { get set }
    var price: Int! {get set}
}
