//
//  PDBillerSmallChange.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDBillerSmallChangeModel {
    var isActive: Bool
    var amount: Int
    
    static let shared = PDBillerSmallChangeModel(isActive: true, amount: 0)
}
