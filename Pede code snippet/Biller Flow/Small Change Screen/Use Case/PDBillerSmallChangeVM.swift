//
//  PDBillerSmallChangeVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDBillerSmallChangeVM {
    
    private let repository: PDBillerSmallChangeRepository
    
    init(repository: PDBillerSmallChangeRepository) {
        self.repository = repository
    }
    
    func transform() {
        //Call Repo Transformer
    }
}
