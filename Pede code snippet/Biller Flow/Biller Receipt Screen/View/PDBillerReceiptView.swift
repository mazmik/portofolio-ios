//
//  PDBillerReceiptView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerReceiptView: BaseView {
    var onCloseTapped: (() -> Void)? { get set }
    var inqury: PDBillerInquiryModel! { get set }
    var result: PDBillerLoadingModelOutputFullInfo! {get set}
    var descriptionText: String! { get set }
    var desc: String! { get set }
}
