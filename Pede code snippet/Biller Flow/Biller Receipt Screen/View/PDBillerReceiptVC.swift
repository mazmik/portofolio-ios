//
//  PDBillerReceiptVC.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PDBillerReceiptVC: UIViewController, PDBillerReceiptView {
    var onCloseTapped: (() -> Void)?
    var inqury: PDBillerInquiryModel!
    var result: PDBillerLoadingModelOutputFullInfo!
    var descriptionText: String!
    var desc: String!
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var transaksiIDLabel: UILabel!
    @IBOutlet weak var jumlahPembayaranLabel: UILabel!
    @IBOutlet weak var tanggalTransaksiLabel: UILabel!
    
    
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var hargaItemLabel: UILabel!
    @IBOutlet weak var deskripsiItemLabel: UILabel!
    
    @IBOutlet weak var biayaLayananLabel: UILabel!
    
    @IBOutlet weak var iurView: UIStackView!
    @IBOutlet weak var biayaIURLabel: UILabel!
    @IBOutlet weak var totalPembayaranLabel: UILabel!
    
    @IBOutlet weak var metodePembayaranLabel: UILabel!
    @IBOutlet weak var metodePembayaranIcon: UIImageView!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        finish()
    }
    
    func setupView() {
        
        setupReceiptView()
        setupModelToView()
//        setupFloattingView()
    }
    
    func setupReceiptView() {
        headerView.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
    }
    
    func setupModelToView() {
        transaksiIDLabel.text = result.ottocashRef
        jumlahPembayaranLabel.text = PDFormatter.currency(number: inqury.payAmount)
        hargaItemLabel.text = PDFormatter.currency(number: inqury.originalPrice)
        let biayaAdmin = inqury.adminFee == 0 ? "Gratis" : PDFormatter.currency(number: inqury.adminFee)
        biayaLayananLabel.text = biayaAdmin
        biayaIURLabel.text = PDFormatter.currency(number: inqury.smallChange.amount)
        totalPembayaranLabel.text = inqury.smallChange.isActive ?
            PDFormatter.currency(number: inqury.originalPrice + inqury.adminFee + inqury.smallChange.amount) : PDFormatter.currency(number: inqury.originalPrice + inqury.adminFee)
        if let token = result.tokenNumber, !token.isEmpty {
            desc = "\(desc ?? "")\nToken: \(token)"
        }
        deskripsiItemLabel.text = desc
        productDescriptionLabel.text = descriptionText
        tanggalTransaksiLabel.text = "Tanggal \(result.date)"
        
        if !inqury.smallChangeSubscribed || !inqury.smallChange.isActive || inqury.smallChange.amount == 0 {
            iurView.isHidden = true
        }
    }
    
    func setupFloattingView() {
        let window = UIApplication.shared.keyWindow!
        
        let buttonParent = UIView(frame: CGRect(origin: CGPoint(x: window.frame.maxX - self.closeButton.frame.size.width - 12, y: 50), size: self.closeButton.frame.size))
        
        buttonParent.addSubview(self.closeButton)
        window.addSubview(buttonParent)
//        window.addSubview(popUpView.popupView)
        finish()
    }
    
    func finish() {
        self.closeButton.rx.tap
            .bind {
                self.onCloseTapped?()
            }
            .disposed(by: disposeBag)
    }

}
