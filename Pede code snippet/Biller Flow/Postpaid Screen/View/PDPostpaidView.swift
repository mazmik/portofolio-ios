//
//  PDPostpaidView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 05/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDPostpaidView: BaseView {
    var onVerified: ((NSDictionary, String) -> Void)? { get set }
    var stringPss : String? { get set }
    var idTrx : Int? { get set }
    var onExpired: (() -> Void)? { get set }
}
