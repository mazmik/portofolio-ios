//
//  PDBillerPostpaidVoucherResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDBillerPostpaidVoucherResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDBillerPostpaidVoucherResponseFullInfo?
}

struct PDBillerPostpaidVoucherResponseFullInfo: Codable {
    let providerCode: String
    let denomination: Int
    let price: Int
    let fee: Int
    let promo: Int
    let description: String
    let productCode: String
}
