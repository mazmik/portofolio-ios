//
//  PDBillerInquiryRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDBillerInquiryRepository {
    
    init() {
        
    }
    
    func billerTransformer(item: BillerInquiryItem) -> PDBillerInquiryModel {
        
        let data = getInquiryData(item: item)
        let smallChange = PDBillerSmallChangeModel(isActive: false,
                                                   amount: data["smallChangeAmount"] as! Int)
        
        let model = PDBillerInquiryModel(inquiryId: data["inquiryId"] as! Int,
                                         transactionAmount: data["transactionAmount"] as! Int,
                                         smallChange: smallChange,
                                         subscriberId: data["subscriberId"] as! String,
                                         subscriberName: data["subscriberName"] as! String,
                                         billNumber: data["billNumber"] as! Int,
                                         adminFee: data["adminFee"] as! Int,
                                         productName: data["productName"] as! String,
                                         originalPrice: data["originalPrice"] as! Int,
                                         payAmount: data["payAmount"] as! Int,
                                         smallChangeSubscribed: data["smallChangeSubscribed"] as! Bool)
        return model
    }
    
    private func getInquiryData(item: BillerInquiryItem) -> NSDictionary {
        return item.responseme!["data"] as! NSDictionary
    }
}
