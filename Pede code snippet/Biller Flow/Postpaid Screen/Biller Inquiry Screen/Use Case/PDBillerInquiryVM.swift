//
//  PDBillerInquiryVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDBillerInquiryVM {
    
    private let repository: PDBillerInquiryRepository
    
    init(repository: PDBillerInquiryRepository) {
        self.repository = repository
    }
    
    func transform(model: BillerInquiryItem) -> PDBillerInquiryModel {
        //Call Repo Transformer
        return repository.billerTransformer(item: model)
    }
}
