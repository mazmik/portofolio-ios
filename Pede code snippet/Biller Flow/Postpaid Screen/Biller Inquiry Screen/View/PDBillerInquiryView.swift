//
//  PDBillerInquiryView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBillerInquiryView: BaseView {
    var model: BillerInquiryItem! { get set }
    var onTermsTapped: (() -> Void)? { get set }
    var onPayTapped: ((PDBillerInquiryModel, String) -> Void)? { get set }
    var onSmallChangeTapped: ((Int) -> Void)? { get set }
}
