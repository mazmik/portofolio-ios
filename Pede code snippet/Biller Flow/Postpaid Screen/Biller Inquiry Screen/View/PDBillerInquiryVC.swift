//
//  PDBillerInquiryVC.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class PDBillerInquiryVC: UIViewController, PDBillerInquiryView {
    
    var onTermsTapped: (() -> Void)?
    var onPayTapped: ((PDBillerInquiryModel, String) -> Void)?
    var onSmallChangeTapped: ((Int) -> Void)?
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var userDetailLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var model: BillerInquiryItem!
    var billerInquiryModel: PDBillerInquiryModel!
    var viewModel: PDBillerInquiryVM!
    var iurbanner = IurBannerVW()
    var isIurEnabled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.billerInquiryModel = viewModel.transform(model: model)
        Preference.set(value: billerInquiryModel.smallChange.amount, forKey: .kPDSMallChange)
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setTitleNavigationBar(title: "Pembayaran")
        self.billerInquiryModel.smallChange.amount = Preference.getInt(forKey: .kPDSMallChange)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    private func setupUI() {
        self.billerInquiryModel.smallChange.isActive = self.billerInquiryModel.smallChangeSubscribed
        setupImageView()
        initTableView()
        checkBanner()
        if let header = model.strhead {
            productDescriptionLabel.text = header.isEmpty ? billerInquiryModel.productName : header
        } else {
            productDescriptionLabel.text = billerInquiryModel.productName
        }
        
        userDetailLabel.text = model.strnohp ?? ""
    }
    
    private func setupImageView() {
        switch model.inTransaksi! {
        case 1..<3:
            customImageView("phone-balance-icon")
        case 4:
            customImageView("tv-subscription-icon")
        case 3, 5, 6, 7:
            customImageView("phone-bill-icon")
        default:
            customImageView("bpjs-bill-icon")
        }
    }
    
    private func customImageView(_ name: String = "phone-balance-icon") {
        let image = UIImage(named: name)
        productImageView.image = image
        productImageView.image = productImageView.image?.withRenderingMode(.alwaysTemplate)
        productImageView.tintColor = .white
    }
    
    private func initTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "DetailPembayaranCell", bundle: nil), forCellReuseIdentifier: "PDDetailPembayaranCell")
    }
    
    @IBAction func onPayTapped(_ sender: UIButton) {
        self.billerInquiryModel.smallChange.amount =
            self.billerInquiryModel.smallChange.isActive ?
                self.billerInquiryModel.smallChange.amount : 0
        self.billerInquiryModel.transactionAmount = getAmount()
        self.onPayTapped?(self.billerInquiryModel, model.strdesc ?? "")
    }
    
    private func getAmount() -> Int {
        return billerInquiryModel.smallChange.isActive ?
            billerInquiryModel.originalPrice + billerInquiryModel.adminFee + billerInquiryModel.smallChange.amount : billerInquiryModel.originalPrice + billerInquiryModel.adminFee
    }
}

extension PDBillerInquiryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PDDetailPembayaranCell", for: indexPath) as? PDDetailPembayaranCell else { return UITableViewCell() }
        
        cell.onClick = {
            self.billerInquiryModel.smallChange.isActive = !self.billerInquiryModel.smallChange.isActive
            self.tableView.reloadData()
        }
        cell.onIur = { [weak self] (isActive) in
            guard let self = self else { return }
            if isActive && self.isIurEnabled {
                self.onSmallChangeTapped?(self.billerInquiryModel.payAmount)
            }
        }
        
        cell.model = billerInquiryModel
        cell.productDescriptionLabel.text = model.strCreditName ?? ""
        let desc = model.inTransaksi! == 1 ? "" : model.strdesc ?? ""
        cell.initLabel(desc, self.billerInquiryModel.smallChange.isActive)
        cell.isIurEnabled = isIurEnabled
        return cell
    }
    
}

extension PDBillerInquiryVC: IurBannerVWDelegate {
    
    func runButton() {
        iurbanner.isHidden = true
    }
    
    func checkBanner() {
        iurbanner  = Bundle.main.loadNibNamed("IurBannerVW", owner: nil, options: nil)?.first as! IurBannerVW
        iurbanner.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: MAX_HEIGHT_HEADER)
        iurbanner.backgroundColor = UIColor(string: COLOR_RED_POP)
        iurbanner.lbl_desc.textColor = UIColor(string: COLOR_WHITE)
        iurbanner.bt_vw.tintColor = UIColor(string: COLOR_WHITE)
        iurbanner.isHidden = false
        guard let data : PDBalanceModel = PDUserBalancePreference().read() else { return }
        let saldoPedeCash = PDFormatter.removeCurrency(string: data.emoney)
        
        if billerInquiryModel.smallChangeSubscribed {
            if saldoPedeCash == 0 {
                iurbanner.lbl_desc.text = "Saldo kurang untuk Investasi Uang Receh"
                iurbanner.bt_vw.setTitle("TUTUP", for: .normal)
                isIurEnabled = false
                
                iurbanner.delegate = self
                iurbanner.addShadow()
                self.mainView.addSubview(iurbanner)
                self.mainView.bringSubview(toFront: iurbanner)
                
            } else {
                isIurEnabled = true
                iurbanner.isHidden = true
            }
        } else {
            iurbanner.isHidden = true
            isIurEnabled = false
        }
    }
}
