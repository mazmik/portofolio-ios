//
//  PDBillerInquiryResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//{
//    "productType": 1,
//    "productCode": "IM3",
//    "billerCode": "ISAT",
//    "productPrice": 11000,
//    "customerReference": "0856123456",
//    "description": "for paying my debt"
//}

struct PDBillerInquiryBody: Codable {
    let productType: Int
    let productCode: String
    let billerCode: String
    let productPrice: Int
    let customerReference: String
    let description: String
}

//{
//    "code": 200,
//    "status": "success",
//    "message": "",
//}

struct PDBillerInquiryResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDBillerInquiryResponseFullInfo?
}

//    "data": {
//        "productName": "indovision",
//        "payAmount": 25003,
//        "adminFee": 1500,
//        "subscriberName": "DONITA RAHMADI",
//        "subscriberId": "010000000033",
//        "unitPrice": 10000,
//        "billNumber": "1 Tagihan",
//        "receiverName": "Donita Rahmadi",
//        "receiverNickname": "donitaRahmadi",
//        "period": "",
//        "merchantName": "KFC Outlet 1",
//        "outletName": "POS 1 at KFC Outlet 1",
//        "originalPrice": 24500,
//        "inquiryId": 10,
//        "smallChangeSubscribed": false,
//        "smallUnitChange": 0.3478,
//        "smallChangeAmount": 1500,
//        "transactionAmount": 1500
//    }

struct PDBillerInquiryResponseFullInfo: Codable {
    let productName: String
    let payAmount: Int
    let adminFee: Int
    let subscriberName: String
    let subscriberId: String
    let unitPrice: Int
    let billNumber: String
    let receiverName: String
    let receiverNickname: String
    let period: String
    let merchantName: String
    let outletName: String
    let originalPrice: Int
    let inquiryId: Int
    let smallChangeSubscribed: Bool
    let smallUnitChange: Double
    let smallChangeAmount: Int
    let transactionAmount: Int
}
