//
//  PDBillerInquiryModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDBillerInquiryModel {
    let inquiryId: Int
    var transactionAmount: Int
    var smallChange: PDBillerSmallChangeModel
    let subscriberId: String
    let subscriberName: String
    let billNumber: Int
    let adminFee: Int
    let productName: String
    let originalPrice: Int
    let payAmount: Int
    let smallChangeSubscribed: Bool
}

enum PDBillerInquiryType: Int {
    case prepaidPhone = 1
    case postpaidPhone = 2
    case waterBill = 3
    case tvBill = 4
    case prepaidElectric = 5
    case postpaidElectric = 6
    case telkomBill = 7
    case bpjs = 11
}

enum PDBillerCodeType {
    case tsel(PDTselType)
    case isat(PDIsatType)
    case asia(PDAsiaType)
    case thre(PDThreType)
    case smart(PDSmrtType)
    case finnet(PDFinnetType)
    case pln(PDPlnType)
    case telkom(PDTelkomType)
    case palyja(PDPalyjaType)
    case aerta(PDAertaType)
    case pamSby(PDPamSbyType)
    case pamSmg(PDPamSmgType)
    
    var code: String {
        switch self {
        case .tsel(_):
            return "TSEL"
        case .isat(_):
            return "ISAT"
        case .asia(_):
            return "ASIA"
        case .thre(_):
            return "THRE"
        case .smart(_):
            return "SMRT"
        case .finnet(_):
            return "FINNET"
        case .pln(_):
            return "PLN"
        case .telkom(_):
            return "TLKM"
        case .palyja(_):
            return "PAL"
        case .aerta(_):
            return "AER"
        case .pamSby(_):
            return "PAMSBY"
        case .pamSmg(_):
            return "PAMSMG"
        }
    }
}

enum PDTselType {
    case postpaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "POST"
        }
    }
}

enum PDTselPostpaidProduct: String {
    case halo = "HALO"
    case simpati = "SIMPATI"
    case AS = "AS"
    
    var prefix: [String] {
        switch self {
        case .halo:
            return ["0811", "0812", "0813"]
        case .simpati:
            return ["0821", "0822"]
        case .AS:
            return ["0823", "0852", "0853", "0851"]
        }
    }
}

enum PDIsatType {
    case postpaid
    case prepaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "POST"
        case .prepaid:
            return "IM3"
        }
    }
}

enum PDIsatPostpaidProduct: String {
    case m2 = "M2"
    case matrix = "Matrix"
    case im3 = "IM3"
    case mentari = "Mentari"
    
    var prefix: [String] {
        switch self {
        case .m2:
            return ["0814"]
        case .matrix:
            return ["0815", "0816", "0855"]
        case .im3:
            return ["0856", "0857"]
        case .mentari:
            return ["0858"]
        }
    }
}

enum PDIsatPrepaidProduct: String {
    case m2 = "M2"
    case matrix = "Matrix"
    case im3 = "IM3"
    case mentari = "Mentari"
    
    var prefix: [String] {
        switch self {
        case .m2:
            return ["0814"]
        case .matrix:
            return ["0815", "0816", "0855"]
        case .im3:
            return ["0856", "0857"]
        case .mentari:
            return ["0858"]
        }
    }
}

enum PDAsiaType {
    case postpaid
    case prepaid
    case tselPrepaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "HX"
        case .prepaid:
            return "HX"
        case .tselPrepaid:
            return "HS"
        }
    }
}

enum PDAsiaPostpaidProduct: String {
    case xl = "XL"
    case axis = "AXIS"
    
    var prefix: [String] {
        switch self {
        case .xl:
            return ["0817", "0818", "0819", "0859", "0877", "0878"]
        case .axis:
            return ["0838", "0831", "0832", "0833"]
        }
    }
}

enum PDAsiaPrepaidProduct: String {
    case xl = "XL"
    case axis = "AXIS"
    
    var prefix: [String] {
        switch self {
        case .xl:
            return ["0817", "0818", "0819", "0859", "0877", "0878"]
        case .axis:
            return ["0838", "0831", "0832", "0833"]
        }
    }
}

enum PDAsiaTselPrepaidProduct: String {
    case halo = "HALO"
    case simpati = "SIMPATI"
    case AS = "AS"
    
    var prefix: [String] {
        switch self {
        case .halo:
            return ["0811", "0812", "0813"]
        case .simpati:
            return ["0821", "0822"]
        case .AS:
            return ["0823", "0852", "0853", "0851"]
        }
    }
}

enum PDThreType {
    case postpaid
    case prepaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "POST"
        case .prepaid:
            return "PRE"
        }
    }
}

enum PDThrePostpaidProduct: String {
    case tri = "Three"
    
    var prefix: [String] {
        switch self {
        case .tri:
            return ["0895", "0896", "0897", "0898", "0899"]
        }
    }
}

enum PDThrePrepaidProduct: String {
    case tri = "Three"
    
    var prefix: [String] {
        switch self {
        case .tri:
            return ["0895", "0896", "0897", "0898", "0899"]
        }
    }
}

enum PDSmrtType {
    case postpaid
    case prepaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "POST"
        case .prepaid:
            return "PRE"
        }
    }
}

enum PDSmartPostpaidProduct: String {
    case smartfren = "Smartfren"
    
    var prefix: [String] {
        switch self {
        case .smartfren:
            return ["0881", "0882", "0883", "0884", "0885", "0886", "0887", "0888", "08849"]
        }
    }
}

enum PDSmartPrepaidProduct: String {
    case smartfren = "Smartfren"
    
    var prefix: [String] {
        switch self {
        case .smartfren:
            return ["0881", "0882", "0883", "0884", "0885", "0886", "0887", "0888", "08849"]
        }
    }
}

enum PDFinnetType {
    case transvision
    case telkomvision
    case toptv
    case yestv
    case okevision
    
    var code: String {
        switch self {
        case .transvision:
            return "TLKMVIS"
        case .telkomvision:
            return "INDOVIS"
        case .toptv:
            return "TOPTV"
        case .yestv:
            return "YESTV"
        case .okevision:
            return "OKEVIS"
        }
    }
}

enum PDPlnType {
    case postpaid
    case prepaid
    
    var code: String {
        switch self {
        case .postpaid:
            return "POST"
        case .prepaid:
            return "PRE"
        }
    }
}

enum PDTelkomType {
    case telkom
    
    var code: String {
        switch self {
        case .telkom:
            return "TLKM"
        }
    }
}

enum PDPalyjaType {
    case palyja
    
    var code: String {
        switch self {
        case .palyja:
            return "PAL"
        }
    }
}

enum PDAertaType {
    case aerta
    
    var code: String {
        switch self {
        case .aerta:
            return "AER"
        }
    }
}

enum PDPamSbyType {
    case pamSby
    
    var code: String {
        switch self {
        case .pamSby:
            return "PAMSBY"
        }
    }
}

enum PDPamSmgType {
    case pamSmg
    
    var code: String {
        switch self {
        case .pamSmg:
            return "PAMSMG"
        }
    }
}
