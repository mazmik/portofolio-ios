//
//  PDBPJSView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 05/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDBPJSView: BaseView {
    var onInquiry: ((BillerInquiryItem) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
}
