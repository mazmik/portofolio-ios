//
//  PDTransactionLoadingView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 01/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTransactionLoadingView: BaseView {
    var onSuccess: ((PDTransactionInquiryModel, PDTransactionLoadingModelOutputFullInfo) -> Void)? { get set }
    var onCanceled: (() -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
    var onInvestmentTapped : (() -> Void)? { get set }
    var onBack: (() -> Void)? { get set }
    var pin: String! { get set }
    var viewModel: PDTransactionLoadingVM! { get set }
    var inqury: PDTransactionInquiryModel! { get set }
}
