//
//  PDTransactionDisableSmallIntroModel.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 11/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDTransactionDisableSmallIntroModelOutput: PDModelOutput {
    typealias output = PDTransactionDisableSmallIntroModelOutputFullInfo
    var meta: PDMeta
    var data: PDTransactionDisableSmallIntroModelOutputFullInfo?
}

struct PDTransactionDisableSmallIntroModelOutputFullInfo {
    
}
