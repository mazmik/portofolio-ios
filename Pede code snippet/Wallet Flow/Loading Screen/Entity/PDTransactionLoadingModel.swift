//
//  PDTransactionLoadingModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDTransactionLoadingModelInput {
    let inquiry: PDTransactionInquiryModel
    let pin: String
}

struct PDTransactionLoadingModelOutput: PDModelOutput {
    typealias output = PDTransactionLoadingModelOutputFullInfo
    var meta: PDMeta
    var data: PDTransactionLoadingModelOutputFullInfo?
}

struct PDTransactionLoadingModelOutputFullInfo {
    let inquiryId: Int
    let smallChangeIncluded: Bool
    let smallChangeAmount: Int
    let transactionAmount: Int
    let ottocashRef: String
    let smallChangeIntro: Bool
    let date: String
}
