//
//  PDTransactionDisableSmallIntroResponse.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 11/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDTransactionDisableSmallIntroResponse: Codable {
    let code: Int
    let status: String
    let message: String
}
