//
//  PDTransactionDisableSmallIntro.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 11/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDTransactionDisableSmallIntroAPI {
    
    private class PostDisableSmallIntroRequest: HTTPRequest {
        var method = HTTPMethods.POST
        var path = "/investments/me/wallets/small-change/disable-intro"
        var apiVersion = ApiVersion.v1
        var parameters: [String : Any] = [:]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request() -> Single<PDTransactionDisableSmallIntroResponse> {
        return httpClient.send(request: PostDisableSmallIntroRequest())
    }
}
