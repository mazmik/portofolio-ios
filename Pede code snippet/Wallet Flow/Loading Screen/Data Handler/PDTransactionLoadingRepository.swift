//
//  PDTransactionLoadingRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDTransactionLoadingRepository {
    
    private let loadingApi: PDTransactionLoadingAPI
    private let disableApi: PDTransactionDisableSmallIntroAPI
    private let disposeBag = DisposeBag()
    
    init(api: PDTransactionLoadingAPI, disableApi: PDTransactionDisableSmallIntroAPI) {
        self.loadingApi = api
        self.disableApi = disableApi
    }
    
    func requestAPI(model: PDTransactionLoadingModelInput) -> Single<PDTransactionLoadingModelOutputFullInfo> {
        return Single.create(subscribe: { (observer) in
            self.loadingApi.request(parameters: self.inputTransformJson(from: model))
                .map({ (response) -> PDTransactionLoadingModelOutput in
                    return self.outputTransformModel(from: response)
                })
                .subscribe(onSuccess: { (model) in
                    switch model.check() {
                    case .success(let result):
                        observer(.success(result))
                    case .failure(let error):
                        observer(.error(error))
                    }
                }, onError: { (err) in
                    observer(.error(err))
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    func disableSmallIntro() -> Completable {
        return Completable.create(subscribe: { (observer) in
            self.disableApi.request()
                .subscribe(onSuccess: { (model) in
                    observer(.completed)
                }, onError: { (err) in
                    observer(.error(err))
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func inputTransformJson(from object: PDTransactionLoadingModelInput) -> [String: Any] {
        let body = PDBillerTransactionBody(inquiryId: object.inquiry.inquiryId,
                                           pin: object.pin,
                                           smallChangeIncluded: object.inquiry.smallChange.isActive,
                                           smallChangeAmount: object.inquiry.smallChange.amount,
                                           transactionAmount: object.inquiry.transactionAmount)
        if let param = body.dictionary {
            return param
        }
        return [String: Any]()
    }
    
    private func outputTransformModel(from object: PDTransactionLoadingResponse) -> PDTransactionLoadingModelOutput {
        let meta = PDMeta(code: object.code, status: object.status, message: object.message)
        
        if let model = object.data {
            let data = PDTransactionLoadingModelOutputFullInfo(inquiryId: model.inquiryId, smallChangeIncluded: model.smallChangeIncluded, smallChangeAmount: model.smallChangeAmount, transactionAmount: model.transactionAmount, ottocashRef: model.ottocashRef, smallChangeIntro: model.smallChangeIntro, date: model.date)
            return PDTransactionLoadingModelOutput(meta: meta, data: data)
        }
        
        return PDTransactionLoadingModelOutput(meta: meta, data: nil)
    }
    
}
