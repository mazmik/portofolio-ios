//
//  PDWalletHistoryVC.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 28/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class PDWalletHistoryVC: UIViewController, PDWalletHistoryView {
    
    var history: [PDWalletModelOutputFullInfo]?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTable()
        self.setupDisplay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setTitleNavigationBar(title: "Riwayat")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupDisplay(){
        self.navigationController?.isNavigationBarHidden = true
        
        if history?.count == 0 {
            self.tableView.isHidden = true
            self.emptyLabel.isHidden = false
        } else {
            self.tableView.isHidden = false
            self.emptyLabel.isHidden = true
        }
    }
    
    func setupTable(){
        self.emptyLabel.isHidden = false
        var nib = UINib.init(nibName: "HistoryTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "HistoryTableViewCell")
        
        nib = UINib.init(nibName: "SimpleTableViewCellHeader", bundle: nil)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: "SimpleTableViewCellHeader")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 300
        self.tableView.estimatedSectionHeaderHeight = 300
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
    }
    
}

extension PDWalletHistoryVC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.history?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.history?[section].transactions.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        
        guard let history = self.history else { return UITableViewCell() }
        let transaction = history[indexPath.section].transactions[indexPath.row]
        
        cell.ammountLabel.text = transaction.amount.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range:nil)
        cell.transactionNameLabel.text = transaction.description
        cell.timeAndTransactionCodeLabel.text = "\(transaction.time) \(transaction.referenceNumber)"
        cell.transactionNoteLabel.text = ""
        
        let amountInt : Int = Int(transaction.amount.filter("-01234567890".contains)) ?? 0
        if amountInt > 0 {
            cell.ammountLabel.textColor = UIColor(string: "#82D96C")
        }
        else{
            cell.ammountLabel.textColor = UIColor(string: "#484848")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SimpleTableViewCellHeader") as! SimpleTableViewCellHeader
        guard let history = history else { return UIView() }
        headerView.titleLabel.text =  "\(history[section].date)"
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
