//
//  PDWalletHistoryView.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 28/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDWalletHistoryView: BaseView {
    var history: [PDWalletModelOutputFullInfo]? { get set }
}
