//
//  ModuleFactoryImpl+Wallet.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 28/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

extension ModuleFactoryImpl: PDWalletModuleFactory {
    func makeWalletView() -> PDWalletView {
        let vc = PDWalletVC()
        vc.viewModel = makeWalletVM()
        return vc
    }
    
    func makeHistoryWalletView() -> PDWalletHistoryView {
        let vc = PDWalletHistoryVC()
        return vc
    }
    
    func makeMerchantNearbyView() -> PDMerchantNearbyView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeNear1Vc") as! PedeNear1VC
        vc.viewModel = makeMerchantNearbyVM()
        return vc
    }
    
    func makeMerchantNearbyDetailView() -> PDMerchantNearbyDetailView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeNear2Vc") as! PedeNear2VC
        return vc
    }
    
    func makeTopupView() -> PDTopupView {
        let vc =  UIStoryboard(name:"TabMenu", bundle:nil).instantiateViewController(withIdentifier: "TopUpGuideVC") as! TopUpGuideVC
        return vc
    }
    
    func makeTransferTemanView() -> PDTransferTemanView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeTrxTransfer1NewVC") as! PedeTrxTransfer1NewVC
        vc.viewModel = makeTransferTemanVM()
        return vc
    }
    
    func makeTransferAmountView() -> PDTranscationAmountView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeTrxTransfer2Vc") as! PedeTrxTransfer2VC
        return vc
    }
    
    func makeInquiryView() -> PDTransactionInquiryView {
        let vc = PDTransactionInquiryVC()
        vc.viewModel = makeTransactionInquiryVM()
        return vc
    }
    
    func makeSmallChangeView() -> PDTransactionSmallChangeView {
        let vc = UIStoryboard(name: "IUR", bundle: nil).instantiateViewController(withIdentifier: "IurUbah1VC") as! IurUbah1VC
        return vc
    }
    
    func makePinView() -> PDTransactionPinView {
        let vc = PDTransactionPinVC()
        return vc
    }
    
    func makeLoadingView() -> PDTransactionLoadingView {
        let vc = PDTransactionLoadingVC()
        vc.viewModel = makeTransactionLoadingVM()
        return vc
    }
    
    func makeReceiptView() -> PDTransactionReceiptView {
        let vc = PDTransactionReceiptVC()
        return vc
    }
    
    func makeQRView() -> PDQRView {
        let vc = UIStoryboard(name:"TabMenu", bundle:nil).instantiateViewController(withIdentifier: "Menu3VC") as! Menu3VC
        return vc
    }
    
    func makeQRTransferView() -> PDQRTransferView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeTrxTransfer2Vc") as! PedeTrxTransfer2VC
        return vc
    }
    
    func makeQRAmountView() -> PDQRAmountView {
        let vc = UIStoryboard(name: "StrPedeTrx", bundle: nil).instantiateViewController(withIdentifier: "PedeTrxQr1Vc") as! PedeTrxQr1VC
        return vc
    }
    
    func makeQRIndomaretView() -> PDQRIndomaretView {
        let vc = UIStoryboard(name: "StrLoading", bundle: nil).instantiateViewController(withIdentifier: "LoadingIDMVc") as! LoadingIDMVC
        return vc
    }
    
    func makeChangedPinSuccessView() -> ChangePinSuccessVC {
        let vc = ChangePinSuccessVC()
        return vc
    }
    
    func makeSettingsPinView() -> PDChangePinVC {
        let vc = PDChangePinVC()
        vc.viewModel = makeChangePinVM()
        return vc
    }
}
