//
//  PDTransactionSmallChange.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDTransactionSmallChangeModel {
    var isActive: Bool
    var amount: Int
    
    static let shared = PDTransactionSmallChangeModel(isActive: true, amount: 0)
}
