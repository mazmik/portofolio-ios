//
//  PDTransactionSmallChangeRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDTransactionSmallChangeRepository {
    
    init() {
        
    }
    
    func smallChangeTransformer(model: PDTransactionSmallChangeModel, amount: Int) -> PDTransactionSmallChangeModel {
        var smallChange = model
        smallChange.amount = amount
        return smallChange
    }
}
