//
//  PDChangePinView.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//


protocol PDChangePinView: BaseView {
    var viewModel: PDChangePinVM! { get set }
    var onFinishChangePin: (() -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
}
