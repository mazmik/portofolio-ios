//
//  PDChangePinVC.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDChangePinVC: UIViewController, PDChangePinView {
    
    var viewModel: PDChangePinVM!
    var onFinishChangePin: (() -> Void)?    
    var onExpired: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var oldPin: PDTextfield!
    @IBOutlet weak var newPin: PDTextfield!
    @IBOutlet weak var reNewPin: PDTextfield!
    @IBOutlet weak var btnSaveNewPin: PDButtonBottom!
    
    var strOldPin: String {
        return oldPin.getValue()
    }
    var strNewPin: String {
        return newPin.getValue()
    }
    var strReNewPin: String {
        return reNewPin.getValue()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupFieldAction()
        setupHideKeyboardWhenTappedAround()
    }
    
    private func setupUI() {
        setupNavigation()
        setupOldPinField()
        setupNewPinField()
        setupReNewPinField()
        setupSaveNewPin()
    }
    
    private func setupNavigation() {
        setTitleNavigationBar(title: "Ganti PIN")
    }
    
    private func setupOldPinField() {
        self.oldPin.build(title: "Kode PIN Lama",
                          hint: "",
                          image: UIImage(named: "ic_field_pin"))
        setActionPinField(viewPin: oldPin)
    }
    
    private func setupNewPinField() {
        self.newPin.build(title: "Kode PIN Baru",
                          hint: "",
                          image: UIImage(named: "ic_field_pin"))
        setActionPinField(viewPin: newPin)
    }
    
    private func setupReNewPinField() {
        self.reNewPin.build(title: "Ulang Kode PIN Baru",
                            hint: "",
                            image: UIImage(named: "ic_field_pin"))
        setActionPinField(viewPin: reNewPin)
    }
    
    private func setupFieldAction() {
        let oldPinValid = self.oldPin.field.rx.text.orEmpty
            .map { $0.validateNumberFormat() && $0.count == 6 }
            .share(replay: 1)
        
        oldPinValid
            .bind(to: self.oldPin.rx.valid)
            .disposed(by: disposeBag)
        
        let pinValid = self.newPin.field.rx.text.orEmpty
            .map { $0.validateNumberFormat() && $0.count == 6 }
            .share(replay: 1)
        
        pinValid
            .bind(to: self.newPin.rx.valid)
            .disposed(by: disposeBag)
        
        let rePinValid = self.reNewPin.field.rx.text.orEmpty
            .map { $0.validateNumberFormat() && $0.count == 6 && $0 == self.newPin.getValue()}
            .share(replay: 1)
        
        rePinValid
            .bind(to: self.reNewPin.rx.valid)
            .disposed(by: disposeBag)
        
        let fieldValid = Observable.combineLatest(oldPinValid, pinValid, rePinValid) { $0 && $1 && $2}
        
        fieldValid
            .bind(to: self.btnSaveNewPin.rx.valid)
            .disposed(by: disposeBag)
    }
    
    private func setActionPinField(viewPin: PDTextfield) {
        let limitPin = 6
        viewPin.validBorderColor = .primaryLightGreen
        viewPin.validIconFieldColor = .primaryLightGreen
        viewPin.field.keyboardType = .numberPad
        viewPin.setSecureField()
        
        viewPin.field.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                if new.count > limitPin {
                    return previous ?? String(new.prefix(limitPin))
                } else {
                    return new
                }
            }
            .subscribe(viewPin.field.rx.text)
            .disposed(by: disposeBag)
    }
    
    private func setupSaveNewPin(){
        self.btnSaveNewPin.set(title: "Simpan")
        self.btnSaveNewPin.button.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.btnSaveNewPin.set(valid: false)
                self.buttonAction()
                APIManager.showLoading()
            })
            .disposed(by: disposeBag)
    }
    
    private func buttonAction() {
        let result = self.viewModel.changePin(oldPin: self.oldPin.getValue(), newPin: self.newPin.getValue())
        result
            .subscribeOn(SchedulerProvider.shared.background)
            .observeOn(SchedulerProvider.shared.main)
            .subscribe { [weak self] (event) in
                guard let self = self else { return }
                self.outputHandler(event: event)
                APIManager.hideLoading()
                self.btnSaveNewPin.set(valid: true)
            }
            .disposed(by: disposeBag)
    }
    
    private func outputHandler(event: Event<PDChangePinModelOutputFullinfo>) {
        switch event {
        case .next(let model):
            print(model)
            self.onFinishChangePin?()
        case .error(let err):
            self.errorHandler(err: err)
        case .completed:
            print("completed")
        }
    }
    
    private func errorHandler(err: Error) {
        PDErrorHandler.shared.isCommon(error: err, onExpired: { [weak self] in
            guard let self = self else { return }
            self.onExpired?()
        }, onLost: { [weak self] (warning) in
            guard let self = self else { return }
            self.popupShowError(message: warning)
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.showErrorMessage(message: error)
        }
    }
}
