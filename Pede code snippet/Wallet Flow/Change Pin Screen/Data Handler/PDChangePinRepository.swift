//
//  PDChangePinRepository.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDChangePinRepository {
    
    private let api: PDChangePinAPI
    private let disposeBag = DisposeBag()
    
    init(api: PDChangePinAPI) {
        self.api = api
    }
    
    func requestAPI(model: PDChangePinModelInput) -> Single<PDChangePinModelOutputFullinfo> {
        return Single.create(subscribe: { (observer) in
            self.api.request(parameters: self.inputTransformJson(from: model))
                .map({ (response) -> PDChangePinModelOutput in
                    return self.outputTransformModel(from: response)
                })
                .subscribe(onSuccess: { (model) in
                    switch model.check() {
                    case .success(let result):
                        observer(.success(result))
                    case .failure(let error):
                        observer(.error(error))
                    }
                }, onError: { (err) in
                    observer(.error(err))
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func inputTransformJson(from object: PDChangePinModelInput) -> [String: Any] {
        let body = PDChangePinBody(oldPin: object.oldPin,
                                   newPin: object.newPin)
        if let param = body.dictionary {
            return param
        }
        return [String: Any]()
    }
    
    private func outputTransformModel(from object: PDChangePinResponse) -> PDChangePinModelOutput {
        let meta = PDMeta(code: object.code, status: object.status, message: object.message)
        
        if let model = object.data {
            let data = PDChangePinModelOutputFullinfo(
                description: model.description,
                isSuccess: model.status == 1 ? true : false)
            return PDChangePinModelOutput(meta: meta, data: data)
        }
        
        return PDChangePinModelOutput(meta: meta, data: nil)
    }
}
