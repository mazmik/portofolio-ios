//
//  PDChangePinAPI.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDChangePinAPI {
    
    private class PDChangePinRequest: HTTPRequest {
        var method = HTTPMethods.PUT
        var path = "/users/me/pin"
        var apiVersion = ApiVersion.v1
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDChangePinResponse> {
        return httpClient.send(request: PDChangePinRequest(parameters: parameters))
    }
}
