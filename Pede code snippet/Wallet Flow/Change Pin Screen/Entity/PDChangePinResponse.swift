//
//  PDChangePinResponse.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//REQUEST
//{
//    "oldPin": "032412",
//    "newPin": "890123"
//}

//RESPONSE
//"data": {
//    "description": "PIN OttoCash sudah diubah",
//    "status": 1
//}

struct PDChangePinBody: Codable {
    let oldPin: String
    let newPin: String
}

struct PDChangePinResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDChangePinResponseFullinfo?
}

struct PDChangePinResponseFullinfo: Codable {
    let description: String
    let status: Int
}
