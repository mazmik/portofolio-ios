//
//  PDChangePinModel.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//REQUEST
//{
//    "oldPin": "032412",
//    "newPin": "890123"
//}

//RESPONSE
//"data": {
//    "description": "PIN OttoCash sudah diubah",
//    "status": 1
//}

struct PDChangePinModelInput {
    let oldPin: String
    let newPin: String
}

struct PDChangePinModelOutput: PDModelOutput {
    typealias output = PDChangePinModelOutputFullinfo
    var meta: PDMeta
    var data: PDChangePinModelOutputFullinfo?
}

struct PDChangePinModelOutputFullinfo {
    let description: String
    let isSuccess: Bool
}
