//
//  PDChangePinVM.swift
//  Pedev20
//
//  Created by clappingApe on 04/07/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDChangePinVM {
    
    private let repository: PDChangePinRepository
    private let disposeBag = DisposeBag()
    
    init(repository: PDChangePinRepository) {
        self.repository = repository
    }
    
    func changePin(oldPin: String, newPin: String) -> PublishSubject<PDChangePinModelOutputFullinfo> {
        let model = PDChangePinModelInput(oldPin: oldPin, newPin: newPin)
        return requestAPI(model: model)
    }
    
    private func requestAPI(model: PDChangePinModelInput) -> PublishSubject<PDChangePinModelOutputFullinfo> {
        let data = PublishSubject<PDChangePinModelOutputFullinfo>()
        
        self.repository.requestAPI(model: model)
            .subscribe { (event) in
                switch event {
                case .error(let err):
                    data.onError(err)
                case .success(let result):
                    if result.isSuccess {
                        data.onNext(result)
                        data.onCompleted()
                    } else {
                        data.onError(ErrorHelper.set(Error: result.description, code: 404))
                    }
                }
            }
            .disposed(by: disposeBag)
        
        return data
    }
}
