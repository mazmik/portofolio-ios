//
//  PDQRIndomaretView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 08/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDQRIndomaretView: BaseView {
    var onInquiry: ((BillerInquiryItem) -> Void)? { get set }
    var onCancel: (() -> Void)? { get set }
    var inquiryID : Int? { get set }
    var strQr: String? { get set }
    var intTrans: Int? { get set }
}
