//
//  PDWalletCoordinatorOutput.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 28/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDWalletCoordinatorOutput {
    var finishFlow: (() -> Void)? { get set }
    var expiredFlow: (() -> Void)? { get set }
    var investmentFlow: (() -> Void)? { get set }
}
