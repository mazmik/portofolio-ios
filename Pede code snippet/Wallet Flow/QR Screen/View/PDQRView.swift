//
//  PDQRView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 02/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDQRView: BaseView {
    var onQRTransferTapped: ((String?, String?) -> Void)? { get set }
    var onQROnlineTapped: ((BillerInquiryItem) -> Void)? { get set }
    var onQROfflineTapped: ((String?) -> Void)? { get set }
    var onQRIndomaretTapped: ((String?, Int?) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
    var isRoot: Bool! { get set }
    var isBack: Bool! { get set }
}
