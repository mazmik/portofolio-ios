//
//  PDWalletCoordinator.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 28/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDWalletCoordinator: BaseCoordinator, PDWalletCoordinatorOutput {
    var finishFlow: (() -> Void)?
    var expiredFlow: (() -> Void)?
    var investmentFlow: (() -> Void)?
    
    private let router: Router
    private let factory: PDWalletModuleFactory
    
    init(router: Router, factory: PDWalletModuleFactory) {
        self.router = router
        self.factory = factory
    }
    
    override func start(with walletMenu: PDWalletMenuEntry) {
        switch walletMenu {
        case .payQR(let menu, let isBack):
            self.showQRScreen(menu: menu, setRoot: true, isBack: isBack)
        case .merchant:
            self.showMerchantScreen(setRoot: true)
        case .topUp:
            self.showTopupScreen(setRoot: true)
        case .p2pTransfer:
            self.showP2pTransfer(setRoot: true)
        case .other:
            self.showWalletScreen()
        }
    }
    
    private func showWalletScreen() {
        let view = factory.makeWalletView()
        view.onMenuTapped = { [weak self] (menu) in
            guard let self = self else { return }
            switch menu {
            case .payQR:
                self.showPayQRScreen()
            case .merchant:
                self.showMerchantScreen()
            case .topUp:
                self.showTopupScreen()
            case .p2pTransfer:
                self.showP2pTransfer()
            case .settingPin:
                self.showSettingsPin()
            default:
                print("Coming Soon")
            }
        }
        view.onHistoryTapped = { [weak self] (history) in
            guard let self = self else { return }
            self.showHistoryScreen(history: history)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.setRootModule(view)
    }
    
    private func showHistoryScreen(history: [PDWalletModelOutputFullInfo]) {
        let view = factory.makeHistoryWalletView()
        view.history = history
        router.push(view)
    }
    
    private func showMerchantScreen(setRoot: Bool = false) {
        let view = factory.makeMerchantNearbyView()
        view.onMerchantDetailTapped = { [weak self] (merchant) in
            guard let self = self else { return }
            self.showMerchantDetailScreen(model: merchant)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view, animated: true, completion: nil)
        }
    }
    
    private func showMerchantDetailScreen(model: PDMerchantNearbyModelOutputFullInfo) {
        let view = factory.makeMerchantNearbyDetailView()
        view.merchant = model
        router.push(view)
    }
    
    private func showTopupScreen(setRoot: Bool = false) {
        let view = factory.makeTopupView()
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.present(view)
        }
    }
    
    private func showP2pTransfer(setRoot: Bool = false) {
        let view = factory.makeTransferTemanView()
        view.onAccountTapped = { [weak self] (name, desc) in
            guard let self = self else { return }
            self.showTransactionAmount(name: name, desc: desc)
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view, animated: true, completion: nil)
        }
    }
    
    private func showTransactionAmount(name: String?, desc: String?) {
        let view = factory.makeTransferAmountView()
        view.nama = name
        view.desc = desc
        view.onTransferTapped = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        router.push(view)
    }
    
    private func showInquiryScreen(item: BillerInquiryItem, setRoot: Bool = false) {
        var description = "", creditName = ""
        if item.strTransaksi!.lowercased().contains(find: "indomaret") {
            description = item.merchantQROffline!
            creditName = item.strhead!
        } else if item.inTransaksi == TRANS_P2P {
            creditName = "Transfer ke \(item.merchantQROffline ?? "")"
        } else if item.inTransaksi == TRANS_QR_ONLINE {
            creditName = "Pembayaran dengan QR Merchant \(item.merchantQROffline ?? "")"
        }
        let view = factory.makeInquiryView()
        view.isRoot = setRoot
        view.model = item
        view.onTransactionTapped = { [weak self] (inquiry, desc) in
            guard let self = self else { return }
            
            self.showPinScreen(inquiry: inquiry, description.elementsEqual("") ? desc : description, creditName: creditName.elementsEqual("") ? item.strCreditName! : creditName)
        }
        view.onSmallChangeTapped = { [weak self] (price) in
            guard let self = self else { return }
            self.showSmallChangeScreen(price)
        }
        view.onFinishCount = { [weak self] in
            guard let self = self else { return }
            self.finishFlow?()
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view)
        }
    }
    
    private func showSmallChangeScreen(_ price: Int) {
        let view = factory.makeSmallChangeView()
        view.price = price
        view.onChanged = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
        }
        router.push(view)
    }
    
    private func showPinScreen(inquiry: PDTransactionInquiryModel, _ desc: String, creditName: String) {
        let view = factory.makePinView()
        view.desc = desc
        view.onSuccess = { [weak self] (pin, desc) in
            guard let self = self else { return }
            self.router.dismissModule()
            self.showLoadingScreen(pin: pin, inquiry: inquiry, desc: desc, creditName: creditName)
        }
        view.onCancel = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
        }
        router.present(view, animated: true, isWrapNavigation: true)
    }
    
    private func showLoadingScreen(pin: String, inquiry: PDTransactionInquiryModel, desc: String, creditName: String) {
        let view = factory.makeLoadingView()
        view.pin = pin
        view.inqury = inquiry
        view.onCanceled = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
        }
        view.onSuccess = { [weak self] (model, result) in
            guard let self = self else { return }
            self.showReceiptScreen(model, result, desc, creditName)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        view.onInvestmentTapped = { [weak self] in
            guard let self = self else { return }
            self.investmentFlow?()
        }
        view.onBack = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
        }
        router.push(view)
    }
    
    private func showReceiptScreen(_ inquiry: PDTransactionInquiryModel, _ result: PDTransactionLoadingModelOutputFullInfo, _ desc: String, _ creditName: String) {
        let view = factory.makeReceiptView()
        view.desc = desc
        view.inqury = inquiry
        view.result = result
        view.descriptionText = creditName
        view.onCloseTapped = { [weak self] in
            guard let self = self else { return }
            self.router.dismissModule()
            self.finishFlow?()
        }
        router.popover(view)
    }
    
    private func showQRScreen(menu: PDWalletQRMenuEntry, setRoot: Bool = false, isBack: Bool = false) {
        switch menu {
        case .scanner:
            self.showPayQRScreen(setRoot: setRoot, isBack: isBack)
        case .offline(let qr):
            self.showQROfflineScreen(qr: qr, setRoot: setRoot)
        case .online(let item):
            self.showInquiryScreen(item: item, setRoot: setRoot)
        case .indomaret(let qr, let inquiryId):
            self.showQRIndomaretScreen(qr: qr, inquiryId: inquiryId, setRoot: setRoot)
        case .amount(let name, let desc):
            self.showQRTransferScreen(name: name, desc: desc, setRoot: setRoot)
        }
    }
    
    private func showPayQRScreen(setRoot: Bool = false, isBack: Bool = false) {
        let view = factory.makeQRView()
        view.isBack = isBack
        view.isRoot = setRoot
        view.onQRTransferTapped = { [weak self] (name, desc) in
            guard let self = self else { return }
            self.showQRTransferScreen(name: name, desc: desc)
        }
        view.onQROnlineTapped = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onQROfflineTapped = { [weak self] (qr) in
            guard let self = self else { return }
            self.showQROfflineScreen(qr: qr)
        }
        view.onQRIndomaretTapped = { [weak self] (qr, inquiryId) in
            guard let self = self else { return }
            self.showQRIndomaretScreen(qr: qr, inquiryId: inquiryId)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view)
        }
    }
    
    private func showQROfflineScreen(qr: String?, setRoot: Bool = false) {
        let view = factory.makeQRAmountView()
        view.strQr = qr
        view.locTrans = TRANS_QR_OFFLINE
        view.intTrans = TRANS_QR_OFFLINE
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view, animated: true, hideBar: false, hideBottomBar: true, completion: nil)
        }
    }
    
    private func showQRIndomaretScreen(qr: String?, inquiryId: Int?, setRoot: Bool = false) {
        let view = factory.makeQRIndomaretView()
        view.strQr = qr
        view.inquiryID = inquiryId
        view.intTrans = TRANS_QR_ONLINE_EMV
        view.onInquiry = { [weak self] (item) in
            guard let self = self else { return }
            if !setRoot {
                self.router.dismissModule()
            }
            self.showInquiryScreen(item: item,setRoot: setRoot)
        }
        view.onCancel = { [weak self] in
            guard let self = self else { return }
            if setRoot {
                self.finishFlow?()
            } else {
                self.router.dismissModule()
            }
        }
        
        if setRoot {
            router.setRootModule(view, hideBar: true)
        } else {
            router.present(view)
        }
    }
    
    private func showQRTransferScreen(name: String?, desc: String?, setRoot: Bool = false) {
        let view = factory.makeQRTransferView()
        view.nama = name
        view.desc = desc
        view.onTransferTapped = { [weak self] (item) in
            guard let self = self else { return }
            self.showInquiryScreen(item: item)
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.expiredFlow?()
        }
        
        if setRoot {
            router.setRootModule(view)
        } else {
            router.push(view, animated: true, hideBar: false, hideBottomBar: true, completion: nil)
        }
    }
    
    private func showSettingsPin() {
        let view = factory.makeSettingsPinView()
        view.onFinishChangePin = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
            self.showChangePinSuccessScreen()
        }
        view.onExpired = { [weak self] in
            guard let self = self else { return }
            self.finishFlow?()
        }
        router.push(view)
    }
    
    private func showChangePinSuccessScreen(){
        let view = factory.makeChangedPinSuccessView()
        view.onFinish = { [weak self] in
            guard let self = self else { return }
            self.router.popModule()
        }
        router.push(view)
    }
}
