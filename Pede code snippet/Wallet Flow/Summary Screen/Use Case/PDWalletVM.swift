//
//  PDWalletVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDWalletVM {
    
    private let repository: PDWalletRepository
    private let disposeBag = DisposeBag()
    
    struct Output {
        let error: Driver<Error?>
        let history: Driver<[PDWalletModelOutputFullInfo]>
    }
    
    init(repository: PDWalletRepository) {
        self.repository = repository
    }
    
    func start() -> PDWalletVM.Output {
        return requestAPI()
    }
    
    private func requestAPI() -> PDWalletVM.Output {
        let error = BehaviorRelay<Error?>(value: nil)
        let history = BehaviorRelay<[PDWalletModelOutputFullInfo]>(value: [])
        
        self.repository.requestAPI()
            .subscribe { (event) in
                switch event {
                case .error(let err):
                    error.accept(err)
                case .success(let model):
                    history.accept(model)
                }
            }
            .disposed(by: disposeBag)
        
        return Output(error: error.asDriver().skip(1),
                      history: history.asDriver().skip(1))
    }
    
    func limitBy3() -> [PDWalletModelOutputFullInfo] {
        return repository.limitBy3()
    }
}
