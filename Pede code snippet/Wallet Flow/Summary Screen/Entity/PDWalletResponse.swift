//
//  PDWalletResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDWalletResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: [PDWalletResponseFullInfo]?
}

struct PDWalletResponseFullInfo: Codable {
    let transactionType: String
    let amount: Int
    let referenceNumber: String
    let description: String
    let id: String
    let valueDate: String
    let status: String
}
