//
//  PDWalletModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDWalletModelOutput: PDModelOutput {
    typealias output = [PDWalletModelOutputFullInfo]
    var meta: PDMeta
    var data: [PDWalletModelOutputFullInfo]?
}

struct PDWalletModelOutputFullInfo {
    let date: String
    let transactions: [PDWalletTransaction]
}

struct PDWalletModel {
    let emoney: String
    let extraIncome: String
    let total: String
    
    init(emoney: Int, extraIncome: Int, total: Int) {
        self.emoney = PDFormatter.currency(number: emoney)
        self.extraIncome = PDFormatter.currency(number: extraIncome)
        self.total = PDFormatter.currency(number: total)
    }
}

struct PDWalletTransaction {
    let referenceNumber: String
    var description: String
    let amount: String
    let status: String
    let type: PDWalletType
    let datetime: String
    
    var identifier: String {
        if status.lowercased().contains(find: "sukses") {
            if let token = status.lowercased().components(separatedBy: "sukses").last {
                var number = Array(token.components(separatedBy: " ").dropFirst())
                number = number.map { $0.replacingOccurrences(of: "(", with: "") }
                number = number.map { $0.replacingOccurrences(of: ")", with: "") }
                return number.joined(separator: " ")
            }
        }
        return ""
    }
    
    var time: String {
        if let timeSplitter = self.datetime.split(separator: " ").last {
            return timeSplitter.lowercased()
        }
        return ""
    }
}

enum PDWalletType {
    case activity
    case history
}

enum PDWalletMenu {
    case payQR
    case merchant
    case topUp
    case p2pTransfer
    case gift
    case splitBill
    case cashOut
    case settingPin
}

enum PDWalletMenuEntry {
    case payQR(PDWalletQRMenuEntry, Bool)
    case merchant
    case topUp
    case p2pTransfer
    case other
}

enum PDWalletQRMenuEntry {
    case scanner
    case online(item: BillerInquiryItem)
    case offline(qr: String?)
    case indomaret(qr: String?, inquiryId: Int?)
    case amount(name: String?, desc: String?)
}

