//
//  WalletHistorycell.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 23/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class WalletHistorycell: UITableViewCell {
    
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var transactionNameLabel: UILabel!
    @IBOutlet weak var ammountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.childView.addShadow3()
        self.childView.layer.cornerRadius = 8
        self.childView.layer.borderWidth = 1
        self.childView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
