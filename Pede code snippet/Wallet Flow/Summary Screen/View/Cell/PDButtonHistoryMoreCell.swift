//
//  PDButtonHistoryMoreCell.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 23/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class PDButtonHistoryMoreCell: UITableViewCell {
    
    var more: () -> Void = {}

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func showMoreClick(_ sender: UIButton) {
        more()
    }
}
