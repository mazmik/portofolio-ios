//
//  PDWalletVC.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PDWalletVC: UIViewController, PDWalletView {
    
    var titleText: String = ""
    var tempHistory: [PDWalletModelOutputFullInfo] = []
    var history: [PDWalletModelOutputFullInfo] = []
    var onHistoryTapped: (([PDWalletModelOutputFullInfo]) -> Void)?
    var onExpired: (() -> Void)?
    var onMenuTapped: ((PDWalletMenu) -> Void)?
    var viewModel: PDWalletVM!
    var status: Bool = false
    @IBOutlet weak var subtitlePedeCash: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var detailHistoryButton: UIButton!
    @IBOutlet weak var lblEmptyHistory: UILabel!
    
    @IBOutlet weak var menuView: UIView!
    
    @IBOutlet weak var payButtonView: PDButtonBottomTitle!
    @IBOutlet weak var merchantButtonView: PDButtonBottomTitle!
    @IBOutlet weak var topUpButtonView: PDButtonBottomTitle!
    @IBOutlet weak var transferButtonView: PDButtonBottomTitle!
    @IBOutlet weak var sendGiftButtonView: PDButtonBottomTitle!
    @IBOutlet weak var splitBillButtonView: PDButtonBottomTitle!
    @IBOutlet weak var withdrawButtonView: PDButtonBottomTitle!
    @IBOutlet weak var setupPinButtonView: PDButtonBottomTitle!
    
    @IBOutlet weak var tableView: UITableView!
    
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        if let userData = PDUserInfoPreference().read(), userData.upgradeStatus.elementsEqual("APPROVED") {
            titleText = "PedeCash Plus"
            status = true
        } else {
            titleText = "PedeCash Reguler"
            status = false
        }
        setupNavigation()
        self.subtitlePedeCash.text = "Saldo \(titleText)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupDisplay()
        self.loadData()
    }
    
    private func setupNavigation() {
        setTitleNavigationBar(title: titleText)
    }
    
    private func loadData() {
        let result = viewModel.start()
        
        result.error
            .drive(onNext: { (error) in
                if let err = error {
                    print(err.localizedDescription)
                    self.errorHandler(err: err)
                }
            })
            .disposed(by: disposeBag)
        
        result.history.asObservable().bind(to: self.rx.history).disposed(by: disposeBag)
        if let data = PDUserBalancePreference().read() {
            self.balanceLabel.text = data.emoney
        }
    }
    
    func setupDisplay(){
        
        self.setupCloseButtonInNavbar()
        self.menuView?.addShadow3()
        
        self.setupPayButton()
        self.setupMerchantButton()
        self.setupTopUpButton()
        self.setupTransferButton()
        self.setupSendGiftButton()
        self.setupSplitBillButton()
        self.setupWithdrawButton()
        self.setupPinButton()
        
        self.view.layoutIfNeeded()
    }
    
    func setupDataHistory(_ model: [PDWalletModelOutputFullInfo]){
        self.setupTable()
        self.tempHistory = viewModel.limitBy3()
        self.history = model
        self.tableView.reloadData()
        
        self.view.layoutIfNeeded()
        if history.count == 0 {
            self.detailHistoryButton.isHidden = false
            self.lblEmptyHistory.isHidden = false
            self.detailHistoryButton.rx.tap.bind {
                self.onHistoryTapped?(self.history)
            }.disposed(by: disposeBag)
        } else {
            self.lblEmptyHistory.isHidden = true
            self.detailHistoryButton.isHidden = true
        }
        self.view.layoutIfNeeded()
    }
    
    func setupTable(){
        self.tableView.register(UINib.init(nibName: "WalletHistoryCell", bundle: nil), forCellReuseIdentifier: "WalletHistoryCell")
        self.tableView.register(UINib.init(nibName: "PDButtonHistoryMoreCell", bundle: nil), forCellReuseIdentifier: "PDButtonHistoryMoreCell")
        self.tableView.register(UINib.init(nibName: "SimpleTableViewCellHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SimpleTableViewCellHeader")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
    }

    func setupPayButton() {
        self.payButtonView.set(image: UIImage(named: "pede-qr-icon")!)
        self.payButtonView.set(title: "Bayar")
        self.payButtonView.setAction {
            self.onMenuTapped?(.payQR)
        }
    }
    
    func setupMerchantButton(){
        self.merchantButtonView.set(image: UIImage(named: "ic_merchantshop")!)
        self.merchantButtonView.set(title: "Merchant\nTerdekat")
        self.merchantButtonView.setAction {
            self.onMenuTapped?(.merchant)
        }
    }
    
    func setupTopUpButton(){
        self.topUpButtonView.set(image: UIImage(named: "pede-top-up-icon")!)
        self.topUpButtonView.set(title: "Top Up")
        self.topUpButtonView.setAction {
            self.onMenuTapped?(.topUp)
        }
    }
    
    func setupTransferButton(){
        self.transferButtonView.set(image: UIImage(named: "transfer-icon")!)
        self.transferButtonView.set(title: "Transfer\nTeman")
        self.transferButtonView.setAction {
            if self.status {
                self.onMenuTapped?(.p2pTransfer)
            } else {
                if let userData = PDUserInfoPreference().read() {
                    let popUp = PDPopup()
                    if userData.upgradeStatus.elementsEqual("PENDING") {
                        popUp.setCustom(view: OnProgressView())
                        popUp.setButton(title: "Mengerti")
                        popUp.closeButton.isHidden = true
                        popUp.show()
                    } else {
                        popUp.show(type: .upgrade, completion: nil)
                    }
                }
            }
            
        }
    }
    
    func setupSendGiftButton(){
        self.sendGiftButtonView.set(image: UIImage(named: "send-gift-icon")!)
        self.sendGiftButtonView.set(title: "Kirim\nHadiah")
        self.sendGiftButtonView.setAction {
//            self.onMenuTapped?(.gift)
            self.showComingsoonPopup()
        }
    }
    
    func setupSplitBillButton(){
        self.splitBillButtonView.set(image: UIImage(named: "split-bill-icon")!)
        self.splitBillButtonView.set(title: "Patungan\nBayar")
        self.splitBillButtonView.setAction {
//            self.onMenuTapped?(.splitBill)
            self.showComingsoonPopup()
        }
    }
    
    func setupWithdrawButton(){
        self.withdrawButtonView.set(image: UIImage(named: "withdraw-icon")!)
        self.withdrawButtonView.set(title: "Tarik\nDuit")
        self.withdrawButtonView.setAction {
//            self.onMenuTapped?(.cashOut)
            self.showComingsoonPopup()
        }
    }
    
    func setupPinButton(){
        self.setupPinButtonView.set(image: UIImage(named: "gear-icon")!)
        self.setupPinButtonView.set(title: "Pengaturan\nPin")
        self.setupPinButtonView.setAction {
            self.onMenuTapped?(.settingPin)
        }
    }
    
    private func showComingsoonPopup() {
        let popup = PDPopup()
        popup.show(type: .comingsoon, completion: nil)
    }
    
    private func errorHandler(err: Error) {
        PDErrorHandler.shared.isCommon(error: err, onExpired: { [weak self] in
            guard let self = self else { return }
            self.onExpired?()
        }, onLost: { [weak self] (warning) in
            guard let self = self else { return }
            self.popupShowError(message: warning)
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.showErrorMessage(message: error)
        }
    }
    
}

extension PDWalletVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tempHistory.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == self.tempHistory.count - 1 ?
            self.tempHistory[section].transactions.count + 1 : self.tempHistory[section].transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == self.tempHistory.count - 1 && indexPath.row == self.tempHistory[indexPath.section].transactions.count {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PDButtonHistoryMoreCell", for: indexPath) as? PDButtonHistoryMoreCell else {return UITableViewCell()}
            cell.more = {
                self.onHistoryTapped?(self.history)
            }
            return cell
        } else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "WalletHistoryCell", for: indexPath) as? WalletHistorycell else {return UITableViewCell()}
            
            let transaction = self.tempHistory[indexPath.section].transactions[indexPath.row]
            cell.ammountLabel.text = transaction.amount.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range:nil)
            let amount : Int = Int(transaction.amount.filter("-01234567890".contains)) ?? 0
            cell.ammountLabel.textColor = amount > 0 ? UIColor(string: "#82D96C") : UIColor(string: "#484848")
            cell.referenceLabel.text = transaction.referenceNumber
            cell.transactionNameLabel.text = transaction.description
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SimpleTableViewCellHeader") as! SimpleTableViewCellHeader
        
        headerView.titleLabel.text = "\(self.history[section].date)"
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 29
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    private func buildButtonCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PDButtonHistoryMoreCell", for: indexPath) as? PDButtonHistoryMoreCell else {return UITableViewCell()}
        cell.more = {
            self.onHistoryTapped?(self.history)
        }
        return cell
    }
}

extension Reactive where Base: PDWalletVC {
    var history: Binder<[PDWalletModelOutputFullInfo]> {
        return Binder(self.base) { view, model in
            view.setupDataHistory(model)
        }
    }
}
