//
//  PDWalletView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDWalletView: BaseView {
    var onHistoryTapped: (([PDWalletModelOutputFullInfo]) -> Void)? { get set }
    var onMenuTapped: ((PDWalletMenu) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
    var viewModel: PDWalletVM! { get set }
}
