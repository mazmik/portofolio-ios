//
//  PDWalletAPI.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDWalletAPI {
    
    private class GetWalletRequest: HTTPRequest {
        var method = HTTPMethods.GET
        var path = "/investments/me/transaction-history"
        var apiVersion = ApiVersion.v1
        var parameters = [String: Any]()
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request() -> Single<PDWalletResponse> {
        return httpClient.send(request: GetWalletRequest())
    }
}
