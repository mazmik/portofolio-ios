//
//  PDWalletRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 24/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDWalletRepository {
    
    private let walletApi: PDWalletAPI
    private let activityApi: PDWalletActivityAPI
    private let disposeBag = DisposeBag()
    
    let dateFormatterToSimpleDate = DateFormatter()
    let dateFormatterToCompleteDate = DateFormatter()
    
    var bucketData: [PDWalletTransaction] = []
    
    init(walletApi: PDWalletAPI, activityApi: PDWalletActivityAPI) {
        self.walletApi = walletApi
        self.activityApi = activityApi
        
        self.dateFormatterToCompleteDate.dateFormat = "dd MM yyyy HH:mm:ss"
        self.dateFormatterToSimpleDate.dateFormat = "dd MMMM yyyy"
    }
    
    func requestAPI() -> Single<[PDWalletModelOutputFullInfo]> {
        let historyResult = self.requestHistoryAPI()
        let activityResult = self.requestActivityAPI()
        
        return Single.create(subscribe: { (observer) in
            let result = Observable.zip(historyResult, activityResult) { (history, activity) in
                return history + activity
            }
            result.subscribe(onNext: { (history) in
                observer(.success(self.group(data: self.bucketData)))
            }, onError: { (err) in
                observer(.error(err))
            })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func requestHistoryAPI() -> Observable<[PDWalletModelOutputFullInfo]> {
        return Observable.create({ (observer) in
            self.walletApi.request()
                .map({ (response) -> PDWalletModelOutput in
                    return self.outputTransformModel(from: response)
                })
                .subscribe(onSuccess: { (model) in
                    switch model.check() {
                    case .success(let result):
                        observer.onNext(result)
                    case .failure(let error):
                        observer.onError(error)
                    }
                }, onError: { (err) in
                    observer.onError(err)
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func requestActivityAPI() -> Observable<[PDWalletModelOutputFullInfo]> {
        return Observable.create({ (observer) in
            
            self.activityApi.request()
                .map({ (response) -> PDWalletModelOutput in
                    return self.outputTransformModel(from: response)
                })
                .subscribe(onSuccess: { (model) in
                    switch model.check() {
                    case .success(let result):
                        observer.onNext(result)
                    case .failure(let error):
                        observer.onError(error)
                    }
                }, onError: { (err) in
                    observer.onError(err)
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func outputTransformModel(from object: PDWalletResponse) -> PDWalletModelOutput {
        let meta = PDMeta(code: object.code, status: object.status, message: object.message)
        
        if let model = object.data {
            //-------
            for mdl in model{
                self.bucketData.append(PDWalletTransaction(referenceNumber: mdl.referenceNumber,
                                                            description: mdl.description,
                                                            amount: PDFormatter.currency(number: mdl.amount),
                                                            status: mdl.status,
                                                            type: .history,
                                                            datetime: mdl.valueDate))
            }
            
            //-------
            let grouped = Dictionary(grouping: model) { (element) -> String in
                return element.valueDate
            }
            
            var models = [PDWalletModelOutputFullInfo]()
            grouped.keys.forEach { (key) in
                if let data = grouped[key] {
                    let trx = data.map({ (response) -> PDWalletTransaction in
                        return PDWalletTransaction(referenceNumber: response.referenceNumber,
                                                    description: response.description,
                                                    amount: PDFormatter.currency(number: response.amount),
                                                    status: response.status,
                                                    type: .history,
                                                    datetime: response.valueDate)
                    })
                    let History = PDWalletModelOutputFullInfo(date: key, transactions: trx)
                    models.append(History)
                }
            }
            return PDWalletModelOutput(meta: meta, data: models)
        }
        
        return PDWalletModelOutput(meta: meta, data: nil)
    }
    
    private func outputTransformModel(from object: PDWalletActivityResponse) -> PDWalletModelOutput {
        let meta = PDMeta(code: object.code, status: object.status, message: object.message)
        
        if let model = object.data {
            //-------
            for mdl in model{
                self.bucketData.append(PDWalletTransaction(referenceNumber: mdl.referenceNumber,
                                                            description: mdl.description,
                                                            amount: PDFormatter.currency(number: mdl.amount),
                                                            status: mdl.status,
                                                            type: .activity,
                                                            datetime: mdl.valueDate))
            }
            
            //-------
            let grouped = Dictionary(grouping: model) { (element) -> String in
                return element.valueDate
            }
            
            var models = [PDWalletModelOutputFullInfo]()
            grouped.keys.forEach { (key) in
                if let data = grouped[key] {
                    let trx = data.map({ (response) -> PDWalletTransaction in
                        return PDWalletTransaction(referenceNumber: response.referenceNumber,
                                                    description: response.description,
                                                    amount: PDFormatter.currency(number: response.amount),
                                                    status: response.status,
                                                    type: .activity,
                                                    datetime: response.valueDate)
                    })
                    let History = PDWalletModelOutputFullInfo(date: key, transactions: trx)
                    models.append(History)
                }
            }
            return PDWalletModelOutput(meta: meta, data: models)
        }
        
        return PDWalletModelOutput(meta: meta, data: nil)
    }
    
    func group(data: [PDWalletTransaction]) -> [PDWalletModelOutputFullInfo] {
        var finalData : [PDWalletModelOutputFullInfo] = []
        let dataValid = validationData(data: data)
        let cal = Calendar.current
        let grouped = Dictionary(grouping: dataValid, by: { cal.startOfDay(for: self.dateFormatterToCompleteDate.date(from: $0.datetime)!) })
        grouped.keys.forEach { (key) in
            let stringDate = self.dateFormatterToSimpleDate.string(from: key)
            let History = PDWalletModelOutputFullInfo(date: stringDate, transactions: grouped[key] ?? [])
            finalData.append(History)
        }
        let sortedData = finalData.sorted(by: { self.stringToDate($0.date) > self.stringToDate($1.date)})
        return sortedData
    }
    
    private func validationData(data: [PDWalletTransaction]) -> [PDWalletTransaction] {
        var historyData = data.filter { $0.type == .history }
        let activityData = data.filter { $0.type == .activity }
        
        for (idx, history) in historyData.enumerated() where history.description.contains(find: "Pembelian") {
            if let activity = activityData.first(where: { $0.referenceNumber == history.referenceNumber }) {
                historyData[idx].description = "\(historyData[idx].description)\nToken: \(activity.identifier)"
            }
        }
        
        return historyData
    }
    
    private func stringToDate(_ string: String) -> Date{
        let date : Date = self.dateFormatterToSimpleDate.date(from: string) ?? Date()
        return date
    }
    
    func limitBy3() -> [PDWalletModelOutputFullInfo] {
        let historyData = Array(self.bucketData.filter { $0.type == .history }.prefix(3))
        let activityData = self.bucketData.filter { $0.type == .activity }
        let data = historyData + activityData
        return self.group(data: data)
    }
}
