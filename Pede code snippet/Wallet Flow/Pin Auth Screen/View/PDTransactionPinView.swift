//
//  PDTransactionPinView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 01/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTransactionPinView: BaseView {
    var onSuccess: ((String, String) -> Void)? { get set }
    var onCancel: (() -> Void)? { get set }
    var desc: String! { get set }
}
