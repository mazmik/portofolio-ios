//
//  PDTransactionPinVC.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 15/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import RxSwift

class PDTransactionPinVC: UIViewController, PDTransactionPinView {
    
    
    @IBOutlet weak var pin1TextField: UnderlineTextField!
    @IBOutlet weak var pin2TextField: UnderlineTextField!
    @IBOutlet weak var pin3TextField: UnderlineTextField!
    @IBOutlet weak var pin4TextField: UnderlineTextField!
    @IBOutlet weak var pin5TextField: UnderlineTextField!
    @IBOutlet weak var pin6TextField: UnderlineTextField!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonThree: UIButton!
    @IBOutlet weak var buttonFour: UIButton!
    @IBOutlet weak var buttonFive: UIButton!
    @IBOutlet weak var buttonSix: UIButton!
    @IBOutlet weak var buttonSeven: UIButton!
    @IBOutlet weak var buttonEight: UIButton!
    @IBOutlet weak var buttonNine: UIButton!
    @IBOutlet weak var buttonZero: UIButton!
    @IBOutlet weak var buttonDel: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
    var onSuccess: ((String, String) -> Void)?
    var onCancel: (() -> Void)?
    var otpFields = [UnderlineTextField]()
    var numberButtons = [UIButton]()
    var activeField: UnderlineTextField?
    var desc: String!
    
    var indexField: Int = 0 {
        didSet {
            self.activeField = self.otpFields[indexField]
        }
    }
    
    private let disposeBag = DisposeBag()
    
    var otpCode: String {
        if let text1 = self.pin1TextField.text,
            let text2 = self.pin2TextField.text,
            let text3 = self.pin3TextField.text,
            let text4 = self.pin4TextField.text,
            let text5 = self.pin5TextField.text,
            let text6 = self.pin6TextField.text {
            return "\(text1)\(text2)\(text3)\(text4)\(text5)\(text6)"
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setTitleNavigationBar(title: "Masukkan Pin")
    }

    
    private func setupUI() {
        setupOTPField()
        setupOTPFieldConfiguration()
        setupPinKeyboard()
        setupDeleteKeyboard()
        setupCancelKeyboard()
    }
    
    private func setupOTPField() {
        otpFields.append(self.pin1TextField)
        otpFields.append(self.pin2TextField)
        otpFields.append(self.pin3TextField)
        otpFields.append(self.pin4TextField)
        otpFields.append(self.pin5TextField)
        otpFields.append(self.pin6TextField)
        self.indexField = 0
    }
    
    private func setupPinKeyboard() {
        numberButtons.append(buttonZero)
        numberButtons.append(buttonOne)
        numberButtons.append(buttonTwo)
        numberButtons.append(buttonThree)
        numberButtons.append(buttonFour)
        numberButtons.append(buttonFive)
        numberButtons.append(buttonSix)
        numberButtons.append(buttonSeven)
        numberButtons.append(buttonEight)
        numberButtons.append(buttonNine)
        
        for (index, button) in numberButtons.enumerated() {
            button.rx.tap
                .subscribe(onNext: { (_) in
                    self.fieldIncrement()
                    if let field = self.activeField {
                        field.text = button.titleLabel?.text ?? "\(index)"
                        self.otpAction()
                    }
                }).disposed(by: disposeBag)
        }
    }
    
    private func setupDeleteKeyboard() {
        buttonDel.rx.tap
            .subscribe(onNext: { (_) in
                if let field = self.activeField {
                    field.text = ""
                    self.fieldDecrement()
                }
            }).disposed(by: disposeBag)
    }
    
    private func setupCancelKeyboard() {
        buttonCancel.rx.tap
            .subscribe(onNext: { (_) in
                self.onCancel?()
            }).disposed(by: disposeBag)
    }
    
    private func fieldIncrement() {
        if self.indexField == 0 && self.otpCode.count < 1 {
            self.indexField = 0
        } else if self.indexField < 6 {
            self.indexField += 1
        }
    }
    
    private func fieldDecrement() {
        if self.indexField != 0 {
            self.indexField -= 1
        }
    }
    
    private func otpAction() {
        if otpCode.count == 6 {
            self.onSuccess?(otpCode, desc)
        }
    }
    
    private func setupOTPFieldConfiguration() {
        for field in otpFields {
            field.focusedBorderColor = .borderGrey
            field.unfocusedBorderColor = .borderGrey
            field.tintColor = .borderGrey
        }
    }
    
}
