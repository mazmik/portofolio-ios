//
//  PDWalletModuleFactory.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 28/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDWalletModuleFactory {
    func makeWalletView() -> PDWalletView
    func makeHistoryWalletView() -> PDWalletHistoryView
    func makeMerchantNearbyView() -> PDMerchantNearbyView
    func makeMerchantNearbyDetailView() -> PDMerchantNearbyDetailView
    func makeTopupView() -> PDTopupView
    func makeTransferTemanView() -> PDTransferTemanView
    func makeTransferAmountView() -> PDTranscationAmountView
    func makeInquiryView() -> PDTransactionInquiryView
    func makeSmallChangeView() -> PDTransactionSmallChangeView
    func makePinView() -> PDTransactionPinView
    func makeLoadingView() -> PDTransactionLoadingView
    func makeReceiptView() -> PDTransactionReceiptView
    func makeQRView() -> PDQRView
    func makeQRTransferView() -> PDQRTransferView
    func makeQRAmountView() -> PDQRAmountView
    func makeQRIndomaretView() -> PDQRIndomaretView
    func makeChangedPinSuccessView() -> ChangePinSuccessVC
    func makeSettingsPinView() -> PDChangePinVC
}
