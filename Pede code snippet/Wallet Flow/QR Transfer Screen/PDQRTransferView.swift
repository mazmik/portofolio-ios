//
//  PDQRTransferView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 08/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDQRTransferView: BaseView {
    var onTransferTapped: ((BillerInquiryItem) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
    var nama : String? { get set }
    var desc : String? { get set }
}
