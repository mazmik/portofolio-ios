//
//  PDTransactionInquiryVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDTransactionInquiryVM {
    
    private let repository: PDTransactionInquiryRepository
    
    init(repository: PDTransactionInquiryRepository) {
        self.repository = repository
    }
    
    func transform(model: BillerInquiryItem) -> PDTransactionInquiryModel {
        //Call Repo Transformer
        return repository.billerTransformer(item: model)
    }
}
