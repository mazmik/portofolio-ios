//
//  PDTransactionInquiryModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//


//"payAmount": 500,
//"productName": "",
//"adminFee": 0,
//"subscriberName": "",
//"subscriberId": "",
//"unitPrice": "",
//"billNumber": 0,
//"receiverName": "",
//"receiverNickname": "",
//"period": "",
//"merchantName": "KFC Outlet 1",
//"outletName": "POS 1 at KFC Outlet 1",
//"inquiryId": 65,
//"smallChangeSubscribed": false,
//"smallChangeUnit": 0.3748,
//"smallChangeAmount": 1500,
//"transactionAmount": 500

struct PDTransactionInquiryModel {
    let inquiryId: Int
    var transactionAmount: Int
    var smallChange: PDTransactionSmallChangeModel
    let subscriberId: String
    let subscriberName: String
    let billNumber: Int
    let adminFee: Int
    let productName: String
    let originalPrice: Int
    let payAmount: Int
    let smallChangeSubscribed: Bool
}

enum PDTransactionInquiryType: Int {
    case qrOffline = 8
    case p2pTransfer = 9
    case qrOnline = 11
}
