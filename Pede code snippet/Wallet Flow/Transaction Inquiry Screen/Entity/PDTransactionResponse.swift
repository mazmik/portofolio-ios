//
//  PDTransactionResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//{
//    "inquiryId": 2,
//    "pin": "123456",
//    "smallChangeIncluded": true,
//    "smallChangeAmount": 1500,
//    "transactionAmount": 12500
//}

struct PDTransactionBody: Codable {
    let productType: Int
    let productCode: String
    let billerCode: String
    let productPrice: Int
    let customerReference: String
    let description: String
}

//{
//    "code": 200,
//    "status": "success",
//    "message": "",
//    "data": {
//        "inquiryId": 3156,
//        "smallChangeIncluded": true,
//        "smallChangeAmount": 1500,
//        "transactionAmount": 12500,
//        "ottocashRef": "8A0500000339",
//        "smallChangeIntro": true,
//        "date": "05/11/2018 13:37:13"
//    }
//}

struct PDTransactionResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDTransactionResponseFullInfo?
}

struct PDTransactionResponseFullInfo: Codable {
    let productName: String
    let payAmount: Int
    let adminFee: Int
    let subscriberName: String
    let subscriberId: String
    let unitPrice: Int
    let billNumber: String
    let receiverName: String
    let receiverNickname: String
    let period: String
    let merchantName: String
    let outletName: String
    let originalPrice: Int
    let inquiryId: Int
    let smallChangeSubscribed: Bool
    let smallUnitChange: Double
    let smallChangeAmount: Int
    let transactionAmount: Int
}
