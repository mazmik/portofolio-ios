//
//  PDTransactionInquiryView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTransactionInquiryView: BaseView {
    var viewModel: PDTransactionInquiryVM! {get set}
    var model: BillerInquiryItem! { get set }
    var onTermsTapped: (() -> Void)? { get set }
    var onTransactionTapped: ((PDTransactionInquiryModel, String) -> Void)? { get set }
    var onSmallChangeTapped: ((Int) -> Void)? { get set }
    var onFinishCount: (() -> Void)? { get set }
    var isRoot: Bool! { get set }
}
