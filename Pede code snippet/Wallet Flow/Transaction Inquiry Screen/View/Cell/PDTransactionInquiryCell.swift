//
//  PDTransactionInquiryCell.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 17/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PDTransactionInquiryCell: UITableViewCell {
    
    @IBOutlet weak var inquiryView: UIView!
    @IBOutlet weak var descView: UIStackView!
    @IBOutlet weak var descriptionProdukLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var servicePriceLabel: UILabel!
    @IBOutlet weak var iurView: UIStackView!
    @IBOutlet weak var iurLabel: UILabel!
    @IBOutlet weak var iurPriceLabel: UILabel!
    @IBOutlet weak var iurButtonCheck: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    var model: PDTransactionInquiryModel?
    var onCountDown: ((String)->Void)?
    
    var isActive = false
    var onClick: (() -> Void)?
    var onIur: ((Bool) -> Void)?
    private let disposeBag = DisposeBag()
    
    var isIurEnabled: Bool! {
        didSet {
            if !self.isIurEnabled {
                self.iurViewDisable()
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initView()
        setupTimerLabel()
        iurLabel.addTapGestureRecognizer {
            self.onIur?(self.model?.smallChange.isActive ?? false)
        }
    }
    
    private func iurViewDisable() {
        iurButtonCheck.isEnabled = false
        iurLabel.isEnabled = false
        iurPriceLabel.isEnabled = false
        let image = UIImage(named: "Ic_uncheck")!
        iurButtonCheck.setImage(image, for: .normal)
        iurLabel.underlinedText(sentence: iurLabel.text!, underlinedWord: "Ubah", color: .lightGray)
    }
    
    private func initView() {
        self.inquiryView.addShadow3()
    }
    
    func initLabel(_ desc: String?, _ isActive: Bool) {
        guard let model = model else { return }
        let smallChangeAmount = Preference.getInt(forKey: .kPDSMallChange)
    
        checkBoxIur(isActive)
        
        descView.isHidden = desc?.isEmpty ?? true
        descriptionProdukLabel.text = desc ?? ""
        productPriceLabel.text = PDFormatter.currency(number: model.payAmount)
        let biayaAdmin = model.adminFee == 0 ? "Gratis" : PDFormatter.currency(number: model.adminFee)
        servicePriceLabel.text = biayaAdmin
        iurPriceLabel.text = PDFormatter.currency(number: smallChangeAmount)
        totalPriceLabel.text = model.smallChange.isActive ?
            PDFormatter.currency(number: model.payAmount + model.adminFee + smallChangeAmount) : PDFormatter.currency(number: model.payAmount + model.adminFee)
        
        iurView.isHidden = !model.smallChangeSubscribed
    }
    
    @IBAction func iurCheckBox(_ sender: UIButton) {
        model!.smallChange.isActive = !model!.smallChange.isActive
        checkBoxIur(model!.smallChange.isActive)
        
        if isIurEnabled {
            onClick?()
        }
    }
    
    private func checkBoxIur(_ isActive: Bool) {
        let image = isActive ? UIImage(named: "ic_inquiry_checlist_active")! : UIImage(named: "Ic_uncheck")!
        iurButtonCheck.setImage(image, for: .normal)
        iurLabel.textColor = isActive ? UIColor.black : UIColor.lightGray
        iurPriceLabel.textColor = isActive ? UIColor.black : UIColor.lightGray
        iurLabel.underlinedText(sentence: iurLabel.text!, underlinedWord: "Ubah", color: isActive ? .blue : .lightGray)
    }
    
    func setupTimerLabel() {
        let counter = PDIndomaretTimerTransaction.shared
        counter.countingAction = { (second) in
            let secondsFormat = PDFormatter.timer(second)
            self.onCountDown?(secondsFormat)
            self.timerLabel.text = "Countdown\n\(secondsFormat)"
        }
    }
    
}
