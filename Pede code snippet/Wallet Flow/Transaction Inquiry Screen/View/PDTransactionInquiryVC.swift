//
//  PDTransactionInquiryVC.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 15/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class PDTransactionInquiryVC: UIViewController, PDTransactionInquiryView {
    
    var viewModel: PDTransactionInquiryVM!
    
    var model: BillerInquiryItem!
    
    var onTermsTapped: (() -> Void)?
    
    var onTransactionTapped: ((PDTransactionInquiryModel, String) -> Void)?
    
    var onSmallChangeTapped: ((Int) -> Void)?
    
    var onFinishCount: (() -> Void)?
    
    var isRoot: Bool!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var userDetailLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var transactionInquiry: PDTransactionInquiryModel!
    var iurbanner = IurBannerVW()
    var isIurEnabled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transactionInquiry = viewModel.transform(model: model)
        Preference.set(value: transactionInquiry.smallChange.amount, forKey: .kPDSMallChange)
        transactionInquiry.smallChange.isActive = transactionInquiry.smallChangeSubscribed
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTitleNavigationBar(title: "Pembayaran")
        if isRoot {
            self.setupCloseButtonInNavbar()
        }
        self.transactionInquiry.smallChange.amount = Preference.getInt(forKey: .kPDSMallChange)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
        
        if model.strhead!.lowercased().contains(find: "indomaret") {
            let counter = PDIndomaretTimerTransaction.shared
            counter.runTimer()
        }
    }
    
    private func setupUI() {
        setupImageView()
        initTableView()
        checkBanner()
        productDescriptionLabel.text = model.strhead ?? ""
        userDetailLabel.text = model.merchantQROffline ?? ""
    }
    
    private func setupImageView() {
        switch model.inTransaksi {
        case TRANS_P2P:
            customImageView("transfer-icon")
        case TRANS_QR_OFFLINE, TRANS_QR_ONLINE:
            customImageView("pede-merchant-icon")
        default:
            customImageView()
        }
    }
    
    private func customImageView(_ name: String = "pede-merchant-icon") {
        let image = UIImage(named: name)
        productImageView.image = image
        productImageView.image = productImageView.image?.withRenderingMode(.alwaysTemplate)
        productImageView.tintColor = .white
    }
    
    private func initTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "PDTransactionInquiryCell", bundle: nil), forCellReuseIdentifier: "PDTransactionInquiryCell")
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func onPayTapped(_ sender: UIButton) {
        self.transactionInquiry.smallChange.amount =
            self.transactionInquiry.smallChange.isActive ?
                self.transactionInquiry.smallChange.amount : 0
        self.transactionInquiry.transactionAmount = getAmount()
        self.onTransactionTapped?(transactionInquiry, model.strdesc ?? "")
    }
    
    private func getAmount() -> Int {
        return transactionInquiry.smallChange.isActive ?
            transactionInquiry.payAmount + transactionInquiry.adminFee + transactionInquiry.smallChange.amount : transactionInquiry.payAmount + transactionInquiry.adminFee
    }
    
}

extension PDTransactionInquiryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PDTransactionInquiryCell", for: indexPath) as? PDTransactionInquiryCell
            else { return UITableViewCell() }
        
        cell.onClick = {
            self.transactionInquiry.smallChange.isActive = !self.transactionInquiry.smallChange.isActive
            self.tableView.reloadData()
        }
        cell.onIur = { [weak self] (isActive) in
            guard let self = self else { return }
            if isActive && self.isIurEnabled {
                self.onSmallChangeTapped?(self.transactionInquiry.payAmount)
            }
        }
        
        if model.strhead!.lowercased().contains(find: "indomaret") {
            cell.timerLabel.isHidden = false
            cell.onCountDown = { (timer) in
                if timer.elementsEqual("00 : 00") {
                    self.onFinishCount?()
                    PDIndomaretTimerTransaction.shared.stop()
                    PDIndomaretTimerTransaction.shared.reset()
                }
                tableView.reloadData()
            }
        } else {
            cell.timerLabel.isHidden = true
        }
        
        cell.productDescriptionLabel.text = model.strCreditName ?? ""
        cell.iurView.isHidden = model.inTransaksi == TRANS_P2P
        cell.initLabel(model.strdesc ?? "", self.transactionInquiry.smallChange.isActive)
        cell.model = transactionInquiry
        cell.isIurEnabled = isIurEnabled
        
        return cell
    }

}

extension PDTransactionInquiryVC: IurBannerVWDelegate {
    
    func runButton() {
        iurbanner.isHidden = true
    }
    
    func checkBanner() {
        iurbanner = Bundle.main.loadNibNamed("IurBannerVW", owner: nil, options: nil)?.first as! IurBannerVW
        iurbanner.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: MAX_HEIGHT_HEADER)
        iurbanner.backgroundColor = UIColor(string: COLOR_RED_POP)
        iurbanner.lbl_desc.textColor = UIColor(string: COLOR_WHITE)
        iurbanner.bt_vw.tintColor = UIColor(string: COLOR_WHITE)
        iurbanner.isHidden = false
        guard let data : PDBalanceModel = PDUserBalancePreference().read() else { return }
        let saldoPedeCash = PDFormatter.removeCurrency(string: data.emoney)
        
        if transactionInquiry.smallChangeSubscribed {
            if saldoPedeCash == 0 {
                iurbanner.lbl_desc.text = "Saldo kurang untuk Investasi Uang Receh"
                iurbanner.bt_vw.setTitle("TUTUP", for: .normal)
                isIurEnabled = false
                
                iurbanner.delegate = self
                iurbanner.addShadow()
                self.mainView.addSubview(iurbanner)
                self.mainView.bringSubview(toFront: iurbanner)
            } else {
                isIurEnabled = true
                iurbanner.isHidden = true
            }
        } else {
            iurbanner.isHidden = true
            isIurEnabled = false
        }
        
        
    }
}
