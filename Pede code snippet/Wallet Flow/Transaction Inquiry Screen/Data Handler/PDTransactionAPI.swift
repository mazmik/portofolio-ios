//
//  PDTransactionAPI.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 24/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
class PDTransactionInquiryAPI {
    
    private class PostTransactionInquiryRequest: HTTPRequest {
        var method = HTTPMethods.POST
        var path = "/investments/me/transaction-inquiry"
        var apiVersion = ApiVersion.v1
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDTransactionResponse> {
        return httpClient.send(request: PostTransactionInquiryRequest(parameters: parameters))
    }
}
