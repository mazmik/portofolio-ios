//
//  PPDTransactionInquiryRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

class PDTransactionInquiryRepository {
    
    init() {
        
    }
    
    func billerTransformer(item: BillerInquiryItem) -> PDTransactionInquiryModel {
        
        let data = getInquiryData(item: item)
        let smallChange = PDTransactionSmallChangeModel(isActive: false,
                                                   amount: data["smallChangeAmount"] as! Int)
        
        let model = inquiryTransform(data: data, smallChange: smallChange)
        
        return model
    }
    
    private func getInquiryData(item: BillerInquiryItem) -> NSDictionary {
        if let response = item.responseme!["data"] {
            return response as! NSDictionary
        }
        return item.responseme!
    }
    
    private func inquiryTransform(data: NSDictionary, smallChange: PDTransactionSmallChangeModel) -> PDTransactionInquiryModel {
        return PDTransactionInquiryModel(inquiryId: data["inquiryId"] as! Int,
                                    transactionAmount: data["transactionAmount"] as! Int,
                                    smallChange: smallChange,
                                    subscriberId: data["subscriberId"] as! String,
                                    subscriberName: data["subscriberName"] as! String,
                                    billNumber: data["billNumber"] as! Int,
                                    adminFee: data["adminFee"] as! Int,
                                    productName: data["productName"] as! String,
                                    originalPrice: data["originalPrice"] as! Int,
                                    payAmount: data["payAmount"] as! Int,
                                    smallChangeSubscribed: data["smallChangeSubscribed"] as! Bool)
    }
}
