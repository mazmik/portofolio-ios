//
//  PDTransactionReceiptView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 29/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTransactionReceiptView: BaseView {
    var onCloseTapped: (() -> Void)? { get set }
    var inqury: PDTransactionInquiryModel! { get set }
    var result: PDTransactionLoadingModelOutputFullInfo! {get set}
    var onInvestmentTapped: (() -> Void)? { get set }
    var descriptionText: String! { get set }
    var desc: String! { get set }
}
