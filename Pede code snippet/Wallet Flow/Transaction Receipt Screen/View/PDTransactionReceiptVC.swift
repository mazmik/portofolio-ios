//
//  PDTransactionReceiptVC.swift
//  Pedev20
//
//  Created by Azmi Muhammad on 15/05/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit
import EasyPopUp
import RxSwift
import RxCocoa

class PDTransactionReceiptVC: UIViewController, PDTransactionReceiptView {
    var onCloseTapped: (() -> Void)?
    var inqury: PDTransactionInquiryModel!
    var result: PDTransactionLoadingModelOutputFullInfo!
    var onInvestmentTapped: (() -> Void)?
    var descriptionText: String!
    var desc: String!
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var transaksiIDLabel: UILabel!
    @IBOutlet weak var jumlahPembayaranLabel: UILabel!
    @IBOutlet weak var tanggalTransaksiLabel: UILabel!
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var hargaItemLabel: UILabel!
    @IBOutlet weak var deskripsiItemLabel: UILabel!
    
    @IBOutlet weak var biayaLayananLabel: UILabel!
    
    @IBOutlet weak var iurView: UIStackView!
    @IBOutlet weak var biayaIURLabel: UILabel!
    @IBOutlet weak var totalPembayaranLabel: UILabel!
    
    @IBOutlet weak var metodePembayaranLabel: UILabel!
    @IBOutlet weak var metodePembayaranIcon: UIImageView!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    
    func setupView() {
        setupReceiptView()
        setupModelToView()
        finish()
    }
    
    func setupReceiptView() {
        headerView.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
    }
    
    func setupModelToView() {
        transaksiIDLabel.text = result.ottocashRef
        jumlahPembayaranLabel.text = PDFormatter.currency(number: inqury.payAmount)
        hargaItemLabel.text = PDFormatter.currency(number: inqury.payAmount)
        let biayaAdmin = inqury.adminFee == 0 ? "Gratis" : PDFormatter.currency(number: inqury.adminFee)
        biayaLayananLabel.text = biayaAdmin
        biayaIURLabel.text = PDFormatter.currency(number: inqury.smallChange.amount)
        totalPembayaranLabel.text = inqury.smallChange.isActive ?
            PDFormatter.currency(number: inqury.payAmount + inqury.adminFee + inqury.smallChange.amount) : PDFormatter.currency(number: inqury.payAmount + inqury.adminFee)
        deskripsiItemLabel.text = desc
        itemLabel.text = descriptionText
        self.tanggalTransaksiLabel.text = "Tanggal \(result.date)"
        
        if !inqury.smallChangeSubscribed || !inqury.smallChange.isActive || inqury.smallChange.amount == 0 {
            iurView.isHidden = true
        }
    }
    
    func setupFloattingView() {
        let window = UIApplication.shared.keyWindow!
        
        let buttonParent = UIView(frame: CGRect(origin: CGPoint(x: window.frame.maxX - self.closeBtn.frame.size.width - 12, y: 50), size: self.closeBtn.frame.size))
        
        buttonParent.addSubview(self.closeBtn)
        window.addSubview(buttonParent)
        //        window.addSubview(popUpView.popupView)
        finish()
    }
    
    func finish() {
        self.closeBtn.rx.tap
            .bind {
                self.closeBtn.removeFromSuperview()
                self.onCloseTapped?()
            }
            .disposed(by: disposeBag)
    }

}
