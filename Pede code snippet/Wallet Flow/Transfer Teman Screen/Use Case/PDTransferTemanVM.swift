//
//  PDTransferTemanVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 23/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDTransferTemanVM {
    
    private let disposeBag = DisposeBag()
    
    struct Output {
        let error: Driver<Error?>
        let contact: Driver<[PDFriendModel]>
    }
    
    init() {
        
    }
    
    func loadContact() -> PDTransferTemanVM.Output {
        let contactRelay = BehaviorRelay<[PDFriendModel]>(value: [])
        let errorRelay = BehaviorRelay<Error?>(value: nil)
        
        PDContactManager.shared.contacts
            .subscribeOn(SchedulerProvider.shared.background)
            .subscribe { (event) in
                switch event {
                case .next(let contact):
                    contactRelay.accept(contact)
                case .error(let error):
                    errorRelay.accept(error)
                case .completed:
                    print("completed")
                }
            }
            .disposed(by: disposeBag)
        
        return Output(error: errorRelay.asDriver().skip(1), contact: contactRelay.asDriver().skip(1))
    }
    
    func filterContact(keyword: String = "") -> PDTransferTemanVM.Output {
        let errorRelay = BehaviorRelay<Error?>(value: nil)
        let result = PDContactManager.shared.filterContacts(keyword: keyword)
        return Output(error: errorRelay.asDriver().skip(1), contact: result.asDriver())
    }
}
