//
//  PDTransferTemanView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 05/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDTransferTemanView: BaseView {
    var onAccountTapped: ((String?, String?) -> Void)? { get set }
    var viewModel: PDTransferTemanVM! { get set }
}
