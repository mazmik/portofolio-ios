//
//  PDQRAmountView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 08/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDQRAmountView: BaseView {
    var onInquiry: ((BillerInquiryItem) -> Void)? { get set }
    var onExpired: (() -> Void)? { get set }
    var locTrans: Int? { get set }
    var intTrans: Int? { get set }
    var strQr: String? { get set }
}
