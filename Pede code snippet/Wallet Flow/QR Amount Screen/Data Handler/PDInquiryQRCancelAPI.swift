//
//  PDInquiryQRCancelAPI.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDInquiryQRCancelAPI {
    
    private class PostInquiryQRCancelRequest: HTTPRequest {
        var method = HTTPMethods.POST
        var path = "/investments/me/qr-cancel"
        var apiVersion = ApiVersion.v2
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDInquiryQRCancelResponse> {
        return httpClient.send(request: PostInquiryQRCancelRequest(parameters: parameters))
    }
}
