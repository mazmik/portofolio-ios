//
//  PDInquiryQRAPI.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDInquiryQRAPI {
    
    private class PostInquiryQRRequest: HTTPRequest {
        var method = HTTPMethods.POST
        var path = "/investments/me/qr-inquiry"
        var apiVersion = ApiVersion.v2
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDInquiryQRResponse> {
        return httpClient.send(request: PostInquiryQRRequest(parameters: parameters))
    }
}
