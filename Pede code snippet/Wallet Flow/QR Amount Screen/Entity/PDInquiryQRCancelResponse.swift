//
//  PDInquiryQRCancelResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//{
//    "inquiryId": 100
//}

struct PDInquiryQRCancelBody: Codable {
    let inquiryId: Int
}



struct PDInquiryQRCancelResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDInquiryQRCancelResponseFullInfo?
}

struct PDInquiryQRCancelResponseFullInfo: Codable {
    
}
