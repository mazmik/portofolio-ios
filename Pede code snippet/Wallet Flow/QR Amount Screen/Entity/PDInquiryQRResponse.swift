//
//  PDInquiryQRResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

//{
//    "qrCode": "RTSM;;;;MG0000000133"
//}

struct PDInquiryQRBody: Codable {
    let qrCode: String
}

//{
//    "code": 200,
//    "status": "success",
//    "message": "",
//    "data": {
//        "billingId": "MG00000001338A0500606179",
//        "merchantId": "fariz123",
//        "merchantCategory": "",
//        "merchantName": "FARIZ",
//        "outletName": "FARIZ",
//        "originalPrice": 200000,
//        "adminFee": 1500
//    }
//}

struct PDInquiryQRResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: PDInquiryQRResponseFullInfo?
}

struct PDInquiryQRResponseFullInfo: Codable {
    let billingId: String
    let merchantId: String
    let merchantCategory: String
    let merchantName: String
    let outletName: String
    let originalPrice: Int
    let adminFee: Int
}
