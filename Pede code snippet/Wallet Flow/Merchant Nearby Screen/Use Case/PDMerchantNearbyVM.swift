//
//  PDMerchantNearbyVM.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 23/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift
import RxCocoa

class PDMerchantNearbyVM {
    
    private let repository: PDMerchantNearbyRepository
    private let disposeBag = DisposeBag()
    
    struct Output {
        let error: Driver<Error?>
        let merchants: Driver<[PDMerchantNearbyModelOutputFullInfo]>
    }
    
    init(repository: PDMerchantNearbyRepository) {
        self.repository = repository
    }
    
    func search(keyword: String = "") -> PDMerchantNearbyVM.Output {
        var model = PDMerchantNearbyModelInput(keyword: keyword)
        let error = BehaviorRelay<Error?>(value: nil)
        let data = BehaviorRelay<[PDMerchantNearbyModelOutputFullInfo]>(value: [])
        
        PDLocationManager.shared.onGetLocation = { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let location):
                model.latitude = location.latitude
                model.longitude = location.longitude
                
                self.repository.requestAPI(model: model)
                    .subscribe { (event) in
                        switch event {
                        case .error(let err):
                            error.accept(err)
                        case .success(let model):
                            data.accept(model)
                        }
                    }
                    .disposed(by: self.disposeBag)
            case .failure(let error):
                print("error: \(error.localizedDescription)")
            }
        }
        return Output(error: error.asDriver().skip(1), merchants: data.asDriver().skip(1))
    }
    
    private func requestAPI(model: PDMerchantNearbyModelInput) -> PDMerchantNearbyVM.Output {
        let error = BehaviorRelay<Error?>(value: nil)
        let data = BehaviorRelay<[PDMerchantNearbyModelOutputFullInfo]>(value: [])
        
        self.repository.requestAPI(model: model)
            .subscribe { (event) in
                switch event {
                case .error(let err):
                    error.accept(err)
                case .success(let model):
                    data.accept(model)
                }
            }
            .disposed(by: disposeBag)
        
        return Output(error: error.asDriver().skip(1), merchants: data.asDriver().skip(1))
    }
}
