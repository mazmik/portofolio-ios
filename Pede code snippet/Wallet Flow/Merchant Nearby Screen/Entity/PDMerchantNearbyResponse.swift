//
//  PDMerchantNearbyResponse.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDMerchantNearbyBody: Codable {
    let longitude: Double
    let latitude: Double
    let explorationType: String
    let offset: Int
    let limit: Int
    let search: String
}

//{
//    "code": 200,
//    "status": "success",
//    "message": "",
//    "data": [
//    {
//    "merchantId": "kfcratuplaza",
//    "customerName": "KFC Ratu Plaza",
//    "address": "Ratu Plaza Lobby Floor",
//    "operationHour": "09.00 - 21.00",
//    "photoUrl": null,
//    "distance": 0.7170331731276437,
//    "latitude": -6.225166,
//    "longitude": 106.801912,
//    "mapUrl": "https://www.google.com/maps/?q=-6.225166,106.801912",
//    "tnc": "Pembayaran Kode QR yang di Scan di Aplikasi Pede&&Tidak ada pembayaran minimum&&Hubungi kami di 021 5060 9500 atau email ke halo@pede.id jika kamu mengalamai Kesulitan",
//    "category": "FnB",
//    "type": "FnB",
//    "province": "DKI Jakarta",
//    "city": "Jakarta Pusat",
//    "district": "Pasar Senen",
//    "village": "Galur"
//    }
//    ]
//}

struct PDMerchantNearbyResponse: Codable {
    let code: Int
    let status: String
    let message: String
    let data: [PDMerchantNearbyResponseFullInfo]?
}

struct PDMerchantNearbyResponseFullInfo: Codable {
    let merchantId: String
    let customerName: String
    let address: String
    let operationHour: String
    let photoUrl: String?
    let distance: Double
    let latitude: Double
    let longitude: Double
    let mapUrl: String
    let tnc: String
    let category: String
    let type: String
    let province: String
    let city: String
    let district: String
    let village: String
}
