//
//  PDMerchantNearbyModel.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 23/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

struct PDMerchantNearbyModelInput {
    var longitude: Double
    var latitude: Double
    let explorationType: String
    var offset: Int
    var limit: Int
    var search: String
    
    init(keyword: String = "") {
        self.longitude = 47.785834
        self.latitude = -117.383179
        self.explorationType = "NEARBY"
        self.offset = 0
        self.limit = 10
        self.search = keyword
    }
}

struct PDMerchantNearbyModelOutput: PDModelOutput {
    typealias output = [PDMerchantNearbyModelOutputFullInfo]
    var meta: PDMeta
    var data: [PDMerchantNearbyModelOutputFullInfo]?
}

struct PDMerchantNearbyModelOutputFullInfo {
    let distance: String
    let longitude: Double
    let latitude: Double
    let terms: [PDMerchantTermsModel]
    let name: String
    let address: String
}

struct PDMerchantTermsModel {
    let content: String
}
