//
//  PDMerchantNearbyView.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 02/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

protocol PDMerchantNearbyView: BaseView {
    var onMerchantDetailTapped: ((PDMerchantNearbyModelOutputFullInfo) -> Void)? { get set }
    var viewModel: PDMerchantNearbyVM! { get set }
    var onExpired: (() -> Void)? { get set }
}
