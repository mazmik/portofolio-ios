//
//  PDMerchantNearbyAPI.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 22/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDMerchantNearbyAPI {
    
    private class GetMerchantNearbyRequest: HTTPRequest {
        var method = HTTPMethods.GET
        var path = "/merchant"
        var apiVersion = ApiVersion.v1
        var parameters: [String: Any]
        var authentication = PDAuthentication.tokenType.bearer
        var header = HeaderType.basic
        
        init(parameters: [String: Any]) {
            self.parameters = parameters
        }
    }
    
    let httpClient: HTTPClient
    
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }
    
    func request(parameters: [String: Any]) -> Single<PDMerchantNearbyResponse> {
        return httpClient.send(request: GetMerchantNearbyRequest(parameters: parameters))
    }
}
