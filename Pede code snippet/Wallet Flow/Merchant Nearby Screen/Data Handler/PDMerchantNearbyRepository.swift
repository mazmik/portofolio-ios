//
//  PDMerchantNearbyRepository.swift
//  Pedev20
//
//  Created by Hedy Pamungkas on 23/04/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import RxSwift

class PDMerchantNearbyRepository {
    
    private let api: PDMerchantNearbyAPI
    private let disposeBag = DisposeBag()
    
    init(api: PDMerchantNearbyAPI) {
        self.api = api
    }
    
    func requestAPI(model: PDMerchantNearbyModelInput) -> Single<[PDMerchantNearbyModelOutputFullInfo]> {
        return Single.create(subscribe: { (observer) in
            self.api.request(parameters: self.inputTransformJson(from: model))
                .map({ (response) -> PDMerchantNearbyModelOutput in
                    return self.outputTransformModel(from: response)
                })
                .subscribe(onSuccess: { (model) in
                    switch model.check() {
                    case .success(let result):
                        observer(.success(result))
                    case .failure(let error):
                        observer(.error(error))
                    }
                }, onError: { (err) in
                    observer(.error(err))
                })
                .disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    private func inputTransformJson(from object: PDMerchantNearbyModelInput) -> [String: Any] {
        let body = PDMerchantNearbyBody(longitude: object.longitude,
                                        latitude: object.latitude,
                                        explorationType: object.explorationType,
                                        offset: object.offset,
                                        limit: object.limit,
                                        search: object.search)
        if let param = body.dictionary {
            return param
        }
        return [String: Any]()
    }
    
    private func outputTransformModel(from object: PDMerchantNearbyResponse) -> PDMerchantNearbyModelOutput {
        let meta = PDMeta(code: object.code, status: object.status, message: object.message)
        
        if let model = object.data {
            let models = model.map { (response) -> PDMerchantNearbyModelOutputFullInfo in
                let distance = PDFormatter.distance(character: response.distance)
                let terms = PDFormatter.separated(character: response.tnc, separator: "&&")
                let termsContent = terms.map({ (term) -> PDMerchantTermsModel in
                    return PDMerchantTermsModel(content: term)
                })
                return PDMerchantNearbyModelOutputFullInfo(distance: distance, longitude: response.longitude, latitude: response.latitude, terms: termsContent, name: response.customerName, address: response.address)
            }
            return PDMerchantNearbyModelOutput(meta: meta, data: models)
        }
        
        return PDMerchantNearbyModelOutput(meta: meta, data: nil)
    }
}
