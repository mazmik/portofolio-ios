//
//  SoldItemCell.swift
//  JualanMu
//
//  Created by Eibiel Sardjanto on 18/11/19.
//  Copyright © 2019 Jualan Mu. All rights reserved.
//

import UIKit

class SoldItemCell: UICollectionViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemDetailView: UIView!
    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var soldQtyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemImage.layer.cornerRadius = 10
        itemDetailView.layer.cornerRadius = 10
        itemDetailView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

}
